import pandas as pd
import altair as alt
import sys
import pathlib

try:
    sys.path.append(str(pathlib.Path(__file__).parents[1]))
except NameError:
    pass

try:
    from preparation.settings import SOURCE_DATA, FILTERS
    from interactive_analysis.shared_specs import NDEP_AXIS, TOOLTIP_HEADERS, HAS_SPECIES_SCALE, CHART_SPECS
except (ImportError, ModuleNotFoundError):
    from src.preparation.settings import SOURCE_DATA, FILTERS
    from src.resources.utils import iso3
    from src.interactive_analysis_species.shared_specs import NDEP_AXIS, TOOLTIP_HEADERS, HAS_SPECIES_SCALE, CHART_SPECS


def ndep_response(headers: pd.DataFrame) -> alt.Chart:
    """
    Build Altiar chart with NDep as horizontal and plot respons vertical: 1-has species 0-does not have species

    """

    return (
        (
            alt.Chart(
                headers.assign(response=headers.has_species.multiply(1)).loc[
                    :,
                    ["response", "has_species"]
                    + [c.split(":")[0] for c in TOOLTIP_HEADERS],
                ]
            ).mark_point(size=40, opacity=0.8, filled=True)
        )
        .encode(
            x=alt.X(NDEP_AXIS["id"], title=NDEP_AXIS["title"],
                    scale=alt.Scale(domain=NDEP_AXIS['range'])),
            y=alt.Y(
                "response:Q",
                title="Plot response [True/False]",
                scale=alt.Scale(domain=[-0.1, 1.1]),
            ),
            tooltip=TOOLTIP_HEADERS,
            color=alt.Color("has_species:N", scale=HAS_SPECIES_SCALE),
        )
        .interactive()
    )


def temperature_ndep(headers: pd.DataFrame, color: str) -> alt.Chart:
    """
    Build Altair chart of temperature plotted against NDep
    :param headers:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                :,
                ["temp_5yr"] + [c.split(":")[0] for c in TOOLTIP_HEADERS],
            ]
        )
        .mark_point(size=40, opacity=0.8, filled=True, fill=color)
        .encode(
            x=alt.X("temp_5yr:Q", title="5 year average temperature [deg C]"),
            y=alt.Y(NDEP_AXIS["id"], title=NDEP_AXIS["title"]),
            tooltip=TOOLTIP_HEADERS,
        )
        .interactive()
    )


def precipitation_ndep(headers: pd.DataFrame, color: str) -> alt.Chart:
    """
    Build Altair chart of precipitation plotted against NDep
    :param headers:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                :,
                ["precip_5yr"] + [c.split(":")[0] for c in TOOLTIP_HEADERS],
            ]
        )
        .mark_point(size=40, opacity=0.8, filled=True, fill=color)
        .encode(
            x=alt.X("precip_5yr:Q", title="5 year average precipitation [mm/dag]"),
            y=alt.Y(NDEP_AXIS["id"], title=NDEP_AXIS["title"]),
            tooltip=TOOLTIP_HEADERS,
        )
        .interactive()
        )


def year_ndep(headers: pd.DataFrame, color: str) -> alt.Chart:
    """
    Build Altair chart of year of observation plotted against NDep
    :param headers:
    :return:
    """
    return (
        alt.Chart(headers.loc[:, [c.split(":")[0] for c in TOOLTIP_HEADERS]])
        .mark_point(size=40, opacity=0.8, filled=True, fill=color)
        .encode(
            x=alt.X(
                "year:Q",
                title="jaar van opname",
                scale=alt.Scale(
                    domain=[FILTERS["earliest_year"] - 5, FILTERS["latest_year"]]
                ),
            ),
            y=alt.Y(NDEP_AXIS["id"], title=NDEP_AXIS["title"]),
            tooltip=TOOLTIP_HEADERS,
        )
        .interactive()
    )
