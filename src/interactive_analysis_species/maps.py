import pandas as pd
import altair as alt
import geopandas as gp
import sys
import pathlib

sys.path.append(str(pathlib.Path(__file__).parents[1]))

try:
    from preparation.settings import SOURCE_DATA
    import shared_specs as specs
except (ImportError, ModuleNotFoundError):
    from src.preparation.settings import SOURCE_DATA, FILTERS
    import src.interactive_analysis_species.shared_specs as specs

alt.data_transformers.enable("json")


def countries_background(source: str) -> alt.Chart:
    """
    Return altair with countries basempa
    :param source: path to source file
    :return:
    """
    countries = gp.read_file(source)
    return (
        alt.Chart(countries)
        .mark_geoshape(fill="lightgrey", stroke="white")
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )


def aoi_map(source: str) -> alt.Chart:
    """
    Return altair AOI
    :param source: path to source file
    :return:
    """
    countries = gp.read_file(source)
    return (
        alt.Chart(countries)
        .mark_geoshape(stroke="darkgrey", fill="darkgrey", fillOpacity=0)
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )


def positive_negative_headers(headers: pd.DataFrame) -> alt.Chart:
    """
    Return Altair map chart with positive and negative headers.
    :param header_db:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                :,
                [
                    "easting",
                    "northing",
                    "has_species",
                ]
                + [c.split(":")[0] for c in specs.TOOLTIP_HEADERS],
            ]
        )
        .mark_point(size=30, opacity=0.75, filled=True)
        .encode(
            latitude="northing:Q",
            longitude="easting:Q",
            color=alt.Color("has_species:N"),  # scale=specs.HAS_SPECIES_SCALE),
            tooltip=specs.TOOLTIP_HEADERS,
        )
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )


def negative_headers(headers: pd.DataFrame) -> alt.Chart:
    """
    Return Altair map chart with positive and negative headers.
    :param headers:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                :,
                [
                    "easting",
                    "northing",
                ]
                + [c.split(":")[0] for c in specs.TOOLTIP_HEADERS],
            ]
        )
        .mark_point(size=30, opacity=0.75, filled=True, color="gray")
        .encode(
            latitude="northing:Q",
            longitude="easting:Q",
            tooltip=specs.TOOLTIP_HEADERS,
        )
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )


def positive_headers(headers: pd.DataFrame) -> alt.Chart:
    """
    Return Altair map chart with positive and negative headers.
    :param headers:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                :,
                [
                    "easting",
                    "northing",
                ]
                + [c.split(":")[0] for c in specs.TOOLTIP_HEADERS],
            ]
        )
        .mark_point(size=30, opacity=0.75, filled=True, color="orange")
        .encode(
            latitude="northing:Q",
            longitude="easting:Q",
            tooltip=specs.TOOLTIP_HEADERS,
        )
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )


def ndep_headers(headers: pd.DataFrame) -> alt.Chart:
    """
    Return Altair map chart with NDep per postive plot
    :param headers:
    :return:
    """

    return (
        alt.Chart(
            headers.loc[
                headers.has_species,
                [
                    "easting",
                    "northing",
                ]
                + specs.TOOLTIP_HEADERS,
            ]
        )
        .mark_point(
            size=30,
            opacity=0.75,
            filled=True,
        )
        .encode(
            latitude="northing:Q",
            longitude="easting:Q",
            color=alt.Color("N_kmol_ha:Q", scale=alt.Scale(scheme="Viridis")),
            tooltip=specs.TOOLTIP_HEADERS,
        )
        .properties(width=specs.MAPS_SPECS["width"], height=specs.MAPS_SPECS["height"])
        .project(type="identity", reflectY=True)
    )
