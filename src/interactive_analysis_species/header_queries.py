import os
import json
import re

import pandas as pd
import numpy as np
import geopandas as gp
from scipy.spatial import cKDTree

import sys
import pathlib

sys.path.append(str(pathlib.Path(__file__).parents[1]))

try:
    from preparation.settings import SOURCE_DATA
    from preparation.header_management_functions import simplify_species
except ImportError:
    from src.preparation.settings import SOURCE_DATA, FILTERS
    from src.resources.utils import iso3
    from src.preparation.header_management_functions import simplify_species


def ckdnearest(gdA: gp.GeoDataFrame, gdB: gp.GeoDataFrame):
    """
    Nearest neighbour search using Scipy cKDTree.
    Courtesy: https://gis.stackexchange.com/questions/222315/geopandas-find-nearest-point-in-other-dataframe
    :param gdA: geodataframe containing origin points, ie negative plots
    :param gdB: geodataframe containing destination points, ie positive plots
    :return: series with gdA.index with nearest ID from gdB,
             series with gdA.index with distance to nearest gdB in KM
    Assumes projected CRS in meters!
    """

    nA = np.array(list(gdA.geometry.apply(lambda x: (x.x, x.y))))
    nB = np.array(list(gdB.geometry.apply(lambda x: (x.x, x.y))))
    btree = cKDTree(nB)
    distances, indexes = btree.query(nA, k=1)

    nearest_ids = pd.Series(
        data=gdB.iloc[indexes].index, index=gdA.index, name="nearestID"
    )
    dist2nearest = pd.Series(
        np.divide(distances, 1000), index=gdA.index, name="dist2nearest"
    )

    return nearest_ids, dist2nearest


class DorenPlots:
    def __init__(self, source_data: dict, prepared_data: dict):

        self.species_df = pd.read_csv(
            prepared_data["eva_species"],
            sep=",",
            usecols=[0, 5, 8],
            header=0,
            names=["plot_obs_id", "matched_concept", "cover_percentage"],
            comment='#',
        )

        # Add simplified species name
        self.species_df['matched_concept_simplified'] = self.species_df.matched_concept.apply(
            simplify_species
        )

        self.header_gdf = gp.read_file(
            f'{os.path.splitext(prepared_data["prepared_headers"])[0]}.shp'
        ).set_index("plot_id", drop=True)

        with open(
            f'{os.path.splitext(prepared_data["prepared_headers"])[0]}.json', "r"
        ) as f:
            self.preparation_menu = json.load(f)
        self.preparation_menu['prepared_headers'] = prepared_data['prepared_headers']

    def get_plots_inventory_for_species(self, species_identifier: str) -> pd.DataFrame:

        pattern = re.compile("s.l.|sl.|var|var.|agg.|agg|aggr.|aggr|sensu")
        sel = self.species_df.query(
            f"matched_concept == '{species_identifier}' or matched_concept_simplified == '{re.split(pattern, species_identifier)[0].strip()}'"
        )
        if not sel.empty:
            print(f'  found {sel.shape[0]} ({len(set(sel.plot_obs_id))}) plots for {species_identifier}')
            return sel
        else:
            raise ValueError(f"No plots found for {species_identifier}")

    def identify_headers_with_species(
        self,
        species_identifier: str,
            shout: bool=False
    ):
        """
        Mark headers in dataframe which contain a species. Adds or overwrites column 'has_species'
        :param species: datatframe with species inventories per plot
        :param species_identifier: name of species to identify
        :param headers: dataframe with headers
        :return: dataframe with headers
        """

        # Match requested species name directly on matched concept column
        plots1 = set(
            self.species_df.query(
                f"matched_concept == '{species_identifier}'"
            ).plot_obs_id
        )

        # Simplify requested species name and search on simplified matched_concept column
        pattern = re.compile(" s\.l\.| sl\. | var | var\. |agg\. | agg | aggr\. | aggr | sensu ")
        plots2 = set(self.species_df.query(
            f"matched_concept_simplified == '{re.split(pattern, species_identifier)[0].strip()}'"
        ).plot_obs_id)

        self.header_gdf = self.header_gdf.assign(
            has_species=self.header_gdf.index.isin(plots1.union(plots2))
        )

        if self.header_gdf.has_species.sum() == 0:
            raise ValueError(f"No plots found for {species_identifier}")

        else:
            if shout:
                print(f'# Found {len(plots1)} plots directly and {len(plots2)} indirectly for {species_identifier}')

    def identify_headers_of_structuurtype(self, strucuurtype: str):
        """ "
        Mark headers in dataframe which belong to the requested structuurtype
        """

        self.header_gdf = self.header_gdf.assign(
            has_structuurtype=self.header_gdf.structuur == strucuurtype
        )

        if self.header_gdf.has_structuurtype.sum() == 0:
            raise ValueError(f"No plots found for {strucuurtype}")

    def identify_nearby(
        self,
        origin_points: gp.GeoDataFrame,
        destination_points: gp.GeoDataFrame,
        search_radius: int,
    ):
        """
        Reduce dataframe to headers within *search_radius* around positive headers

        :param gdf:
        :param search_radius: search radius in kilometer!
        :return:
        """

        nearest_ids, dist_2_nearest = ckdnearest(
            gdA=origin_points, gdB=destination_points
        )
        self.header_gdf["nearest_id"] = nearest_ids.astype(int)
        self.header_gdf["dist_2near"] = dist_2_nearest
        self.header_gdf["within_buffer"] = (
            self.header_gdf["dist_2near"].fillna(0) <= search_radius
        )

    def filter_to_same_structure(self):
        """
        Filter the dataframe to positive plots AND negative plots that have identical Structuurtype as the nearest positive plot
        :param gdf:
        :return:
        """

        self.header_gdf["type_of_nearest"] = (
            self.header_gdf["nearest_id"]
            .fillna(0)
            .astype(int)
            .map(self.header_gdf["structuur"].to_dict() | {0: np.nan})
        )
        self.header_gdf["type_as_nearest"] = (
            self.header_gdf.structuur == self.header_gdf.type_of_nearest
        ) | self.header_gdf.type_of_nearest.isna()

    def select_for_species(
        self, identifier: str, search_radius: int, structuurtype: str, minimum: int,
    ) -> pd.DataFrame:
        """
        Return a headers that are either:
          positive: containing the species and of structuurtype
          negative: not containing the species, but of equal structuurtype and within 25km radius

        :param identifier: species identifier
        :param search_radius: radius around positive plots to find negative plots in
        :param structuurtype: a structuurtype to which both negative and positive plots must comply
        :param minimum: minimum nr of plots needed to proceed
        :return:
        """

        print(f"querying for {identifier} and {structuurtype}")
        self.identify_headers_with_species(species_identifier=identifier)
        self.identify_headers_of_structuurtype(strucuurtype=structuurtype)

        if self.header_gdf.query("has_species & has_structuurtype").empty:
            raise ValueError(
                f"No intersecting headers for {identifier} - {structuurtype}\n"
            )

        if self.header_gdf.query("has_species & has_structuurtype").shape[0] < minimum:
            raise ValueError(
                f'Insufficient headers found: {self.header_gdf.query("has_species & has_structuurtype").shape[0]} (at least {minimum} requested).'
            )

        self.identify_nearby(
            origin_points=self.header_gdf.loc[
                ~(self.header_gdf["has_species"])
                & (self.header_gdf["has_structuurtype"])
            ],
            destination_points=self.header_gdf.loc[
                (self.header_gdf["has_species"])
                & (self.header_gdf["has_structuurtype"])
            ],
            search_radius=search_radius,
        )

        # Return only headers that:
        #   have requested structuurtype
        #   have the species OR are within the buffer
        out = pd.DataFrame(
            self.header_gdf.loc[
                (self.header_gdf.has_structuurtype)
                & ((self.header_gdf.has_species) | (self.header_gdf.within_buffer)),
                :,
            ].drop("geometry", axis=1)
        ).reset_index()
        print(f"found {out.shape[0]} EVA headers")
        return out
