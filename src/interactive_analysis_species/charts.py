import pandas as pd
import altair as alt
import sys
import pathlib
import numpy as np

try:
    sys.path.append(str(pathlib.Path(__file__).parents[1]))
except NameError:
    pass

try:
    from preparation.settings import SOURCE_DATA
    from shared_specs import (
        eunis_colors,
        soil_type_colors,
        NDEP_AXIS,
        TOOLTIP_HEADERS,
        HAS_SPECIES_SCALE,
    )
    from preparation.header_management_functions import eunis_code_to_type
except (ImportError, ModuleNotFoundError):
    from src.preparation.settings import SOURCE_DATA, FILTERS
    from src.interactive_analysis_species.shared_specs import (
        eunis_colors,
        soil_type_colors,
        NDEP_AXIS,
        TOOLTIP_HEADERS,
        HAS_SPECIES_SCALE,
    )
    from src.preparation.header_management_functions import eunis_code_to_type

alt.data_transformers.enable("json")


def structuurtypen_donut(headers: pd.DataFrame) -> alt.Chart:
    """
    Return pie-chart with structuurtypen of headers
    :param headers:
    :return:
    """

    df = pd.DataFrame(
        {
            "category": headers.structuur.value_counts().index,
            "count": headers.structuur.value_counts().values,
        }
    )

    base = alt.Chart(df).encode(
        theta=alt.Theta("count:Q", stack=True), color=alt.Color("category:N")
    )
    pie = base.mark_arc(outerRadius=120, innerRadius=50)
    text = base.mark_text(radius=140, size=15).encode(text="count:Q")
    return pie + text


def bodemtype_donut(headers: pd.DataFrame) -> alt.Chart:
    """
    Return pie-chart with bodemtype
    """
    df = pd.DataFrame(
        {
            "category": headers.soil_type_.value_counts().index,
            "count": headers.soil_type_.value_counts().values,
        }
    )

    base = alt.Chart(df).encode(
        theta=alt.Theta("count:Q", stack=True),
        color=alt.Color(
            "category:N",
            scale=alt.Scale(
                range=[soil_type_colors[soil_type] for soil_type in df.category],
                domain=df.category.to_list(),
            ),
        ),
    )

    pie = base.mark_arc(outerRadius=120, innerRadius=50)
    text = base.mark_text(radius=140, size=15).encode(text="count:Q")
    return pie + text


def eunis_donut(headers: pd.DataFrame) -> alt.Chart:
    """
    Return pie-chart with bodemtype
    """
    df = pd.DataFrame(
        {
            "category": headers.eunis_code.value_counts().index.map(
                eunis_code_to_type()
            ),
            "count": headers.eunis_code.value_counts().values,
        }
    )

    base = alt.Chart(df).encode(
        theta=alt.Theta("count:Q", stack=True),
        color=alt.Color(
            "category:N",
            scale=alt.Scale(
                range=[eunis_colors[eunis_code] for eunis_code in df.category],
                domain=df.category.to_list(),
            ),
        ),
    )
    pie = base.mark_arc(outerRadius=120, innerRadius=50)
    text = base.mark_text(radius=140, size=15).encode(text="count:Q")
    return pie + text


def headers_to_binned_prob(headers: pd.DataFrame, n_bins: int = 10) -> pd.DataFrame:
    """ """


def bins_to_labels(bins: np.array) -> list:
    """ """

    bin_indices = list(range(1, bins.size + 1))
    actual_labels = [f"{bins[i]} ≤ N < {bins[i+1]}" for i in range(bins.size - 1)] + [
        f">= {bins[-1]}"
    ]
    d = dict(zip(bin_indices, actual_labels))
    expr = ""
    for i in bin_indices[:-1]:
        expr += f"datum.label == {i} ? '{d[i]}' : "
    expr += f"'{d[bin_indices[-1]]}'"
    return expr


def response_bar_chart(headers: pd.DataFrame, n_bins: int) -> alt.Chart:
    """
    Bin headers into an NDepostion bin.
    Build Altair Chart showing NDep bins on horizontal axis, with dual barplot for positive/negative plot-count at each bin.
    Also calculate probabiltiy for a positive plot at each bin as:
      # positive plots @ bin / (# positive + # negative plots @ bin).
    Show probability per bin on second vertical axis

    Parameters
    ----------
    headers
    n_bins

    Returns
    -------

    """

    # Create bins and assign bin number to each header
    bins = np.linspace(0, 10, num=n_bins, endpoint=False)
    headers["ndep_bin"] = np.digitize(headers.N_kmol_ha, bins=bins)

    # Pivot on NDep bins to calculate plot counts per bin
    piv = pd.pivot_table(
        headers,
        index="ndep_bin",
        columns="has_species",
        values="plot_id",
        aggfunc="count",
    ).rename(columns={True: "positive", False: "negative"})
    piv["total"] = piv.sum(axis=1)
    piv["prob"] = np.multiply(np.divide(piv.positive, piv.total), 100).round(1)

    # Merge with second dataframe to ensure all bins are mentioned
    piv = (
        piv.reset_index()
        .merge(
            pd.DataFrame({"ndep_bin": range(1, n_bins + 1)}),
            how="right",
            left_on="ndep_bin",
            right_on="ndep_bin",
        )
        .fillna(0)
    )

    # Build string labels
    labels = bins_to_labels(bins)

    # Determine vertical axis limits. Left=plot counts. Right=probability
    y_lim_counts = np.multiply(
        np.ceil(np.divide(piv.loc[:, ["positive", "negative"]].max().max(), 100)), 100
    )
    y_lim_prob = np.multiply(np.ceil(np.divide(piv.prob.max(), 25)), 25)

    # Base chart with N-deposition bins horizontal axis and custom labels
    base = alt.Chart(piv).encode(
        x=alt.X(
            "ndep_bin:O",
            title=NDEP_AXIS["title"],
            axis=alt.Axis(
                labels=True,
                labelExpr=(labels),
                values=[i for i in range(1, n_bins + 1)],
                labelAngle=45,
                labelFontSize=12,
            ),
        )
    )

    # Barchart for negative plots
    bars_negative = base.mark_bar(opacity=0.5, fill="gray").encode(
        y=alt.Y(
            "negative:Q", scale=alt.Scale(domain=[0, y_lim_counts]), title="# plots"
        ),
    )
    # Bars for positive plots
    bars_positive = base.mark_bar(fill="orange", opacity=0.5).encode(
        y=alt.Y("positive:Q", scale=alt.Scale(domain=[0, y_lim_counts])),
    )
    # Points for probalities
    pnts = base.mark_point().encode(
        y=alt.Y("prob:Q", scale=alt.Scale(domain=[0, y_lim_prob]), title="probability"),
        tooltip=["positive", "negative", "total", "prob"],
    )
    # Line connecting points
    line = base.mark_line().encode(
        y=alt.Y("prob:Q", scale=alt.Scale(domain=[0, y_lim_prob])),
    )
    return alt.layer((bars_negative + bars_positive), (pnts + line)).resolve_scale(
        y="independent"
    )


def meteo_boxplot_binned(
    headers: pd.DataFrame, n_bins: int, meteo_var: str
) -> alt.Chart:
    """
    Return Altair Chart with box/whisker plots showing distirbution of meteorological variable, with
    dual boxplot for each NDep bin: positive and negative

    Parameters
    ----------
    headers
    n_bins
    meteo_var

    Returns
    -------

    """

    # Create bins and assign bin number to each header
    bins = np.linspace(0, 10, num=n_bins, endpoint=False)
    headers["ndep_bin"] = np.digitize(headers.N_kmol_ha, bins=bins)

    # Add row for each missing bin, with dummy values for all other columns
    missing_bins = list(set(range(1, n_bins + 1)).difference(set(headers.ndep_bin)))
    dummy_values = {np.dtype('int64'): 0,
                    np.dtype('O'): '',
                    np.dtype('float64'): 0.0,
                    np.dtype('bool'): False}
    headers = pd.concat([headers,
                         pd.DataFrame(data=[headers.dtypes.map(dummy_values)] * len(missing_bins),
                                      columns=headers.columns).assign(ndep_bin=missing_bins)])

    # Nudge positive/ngative plots around ndep-bin
    headers["ndep_bin_nudge"] = np.where(
        headers.has_species, headers.ndep_bin - 0.1, headers.ndep_bin + 0.1
    )

    box = (
        alt.Chart(headers)
        .mark_boxplot(extent="min-max")
        .encode(
            x=alt.X(
                "ndep_bin_nudge:Q",
                title=NDEP_AXIS["title"],
                axis=alt.Axis(
                    labels=True,
                    labelExpr=(bins_to_labels(bins)),
                    values=[i for i in range(1, n_bins + 1)],
                    labelAngle=45,
                    labelFontSize=12,
                ),
            ),
            y=f"{meteo_var}:Q",
            color="has_species:N",
        )
    )

    return box

import datapane as dp

app = dp.App("# Binned Boxplots", dp.Plot(box, caption="boxplot per N-deposition bin"))
app.save(r"c:\apps\proj_code\doren22\test_02.html")
