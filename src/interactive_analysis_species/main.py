import datetime
import os
import pathlib
import sys

import altair as alt
import datapane as dp

try:
    sys.path.append(str(pathlib.Path(__file__).parents[1]))
except NameError:
    pass

try:
    from preparation.header_management_functions import slugify, species_name_translator
    from preparation.settings import FILTERS, PREPARED_DATA, SOURCE_DATA

    from shared_specs import has_species_colors
    from charts import (
        bodemtype_donut,
        eunis_donut,
        structuurtypen_donut,
        response_bar_chart,
    )
    from header_queries import DorenPlots
    from maps import aoi_map, countries_background, negative_headers, positive_headers
    from scatterplots import (
        precipitation_ndep,
        temperature_ndep,
        year_ndep,
        ndep_response,
    )
    from tables import preparation_menu

except (ImportError, ModuleNotFoundError) as e:
    from src.interactive_analysis_species.charts import (
        bodemtype_donut,
        eunis_donut,
        structuurtypen_donut,
        response_bar_chart,
    )
    from src.interactive_analysis_species.header_queries import DorenPlots
    from src.interactive_analysis_species.maps import (
        aoi_map,
        countries_background,
        negative_headers,
        positive_headers,
    )
    from src.interactive_analysis_species.scatterplots import (
        precipitation_ndep,
        temperature_ndep,
        year_ndep,
        ndep_response,
    )
    from src.interactive_analysis_species.shared_specs import has_species_colors
    from src.interactive_analysis_species.tables import preparation_menu
    from src.preparation.header_management_functions import (
        slugify,
        species_name_translator,
    )
    from src.preparation.settings import FILTERS, PREPARED_DATA, SOURCE_DATA


def build_app(plots: DorenPlots, identifier: str, structuurtype: str,
              minimum: int,
              search_radius: int):
    """

    Parameters
    ----------
    plots: DorenPlots item with headers and species lists
    identifier: Species name (Latin)
    structuurtype: One of structuurtypen
    minimum: least nr of positive plots to proceed. Default 1
    search_radius: radius around positive plots to search for negative plots with same structuurtype

    Returns
    -------

    """

    alt.data_transformers.enable("json")

    plantje = identifier
    # plantje = r'Potentilla erecta'
    # structuurtype = 'nat grasland'
    plantje_nl = species_name_translator(
        identifier=plantje,
        is_what="wetenschappelijke_naam",
        return_as="nederlandse_naam",
    )

    print(f"building App for {plantje_nl} ({plantje})")

    selected_headers = plots.select_for_species(
        identifier=plantje, search_radius=search_radius, structuurtype=structuurtype,
        minimum=minimum,
    )

    # Tables
    prep_menu = preparation_menu(plots, buffer_distance=search_radius, structuurtype=structuurtype)

    # Maps
    country_background = countries_background(
        source=SOURCE_DATA["country_background_map"]
    )
    aoi = aoi_map(source=plots.preparation_menu["source_data"]["aoi"])
    positive_header_map = positive_headers(
        headers=selected_headers.loc[selected_headers.has_species]
    )
    negative_header_map = negative_headers(
        headers=selected_headers.loc[~selected_headers.has_species]
    )

    # Scatterplots
    temperature_positive = temperature_ndep(
        headers=selected_headers.loc[selected_headers.has_species],
        color=has_species_colors[True],
    )
    temperature_negative = temperature_ndep(
        headers=selected_headers.loc[~selected_headers.has_species],
        color=has_species_colors[False],
    )
    precipitation_positive = precipitation_ndep(
        headers=selected_headers.loc[selected_headers.has_species],
        color=has_species_colors[True],
    )
    precipitation_negative = precipitation_ndep(
        headers=selected_headers.loc[~selected_headers.has_species],
        color=has_species_colors[False],
    )
    yearly_positive = year_ndep(
        headers=selected_headers.loc[selected_headers.has_species],
        color=has_species_colors[True],
    )
    yearly_negative = year_ndep(
        headers=selected_headers.loc[~selected_headers.has_species],
        color=has_species_colors[False],
    )
    ndep_response_chart = ndep_response(headers=selected_headers)

    # Graphs
    response_density_chart = response_bar_chart(headers=selected_headers, n_bins=20)

    soil_types_positive = bodemtype_donut(
        headers=selected_headers.loc[selected_headers.has_species]
    )
    soil_types_negative = bodemtype_donut(
        headers=selected_headers.loc[~selected_headers.has_species]
    )

    eunis_positive = eunis_donut(
        headers=selected_headers.loc[selected_headers.has_species]
    )
    eunis_negative = eunis_donut(
        headers=selected_headers.loc[~selected_headers.has_species]
    )

    app = dp.App(
        "# DOREN 2023",
        f"DOREN analyse van EVA headers voor {plantje_nl} (__{plantje}__) in {structuurtype}. ",
        # Tables
        "## Selection process",
        prep_menu,
        # Map
        "## Header map and AOI",
        dp.Plot(
            country_background + aoi + negative_header_map + positive_header_map,
            caption=f"Locations of {selected_headers.has_species.value_counts()[True]:,} positive headers for {plantje} - {structuurtype} and negative headers within 25 km distance.",
        ),
        "## Scatterplots",
        dp.Plot(
            temperature_negative + temperature_positive,
            caption="Relation average temperature and N-dep",
        ),
        dp.Plot(
            precipitation_negative + precipitation_positive,
            caption="Relation average precipitation and N-dep",
        ),
        dp.Plot(yearly_negative + yearly_positive, caption="N-dep per year"),
        "## Response density charts",
        dp.Plot(ndep_response_chart, caption="Plot respons to N-deposition"),
        dp.Plot(response_density_chart, caption="Plot response density chart."),

        "## Verdeling over bodemtypen",
        dp.Plot(
            soil_types_positive,
            caption=f"Verdeling van {selected_headers.has_species.value_counts()[True]} positive headers over bodemtypen.",
        ),
        dp.Plot(
            soil_types_negative,
            caption=f"Verdeling van van {selected_headers.has_species.value_counts()[False]} negatieve headers over bodemtypen.",
        ),
        "## Verdeling over EUNIS typen",
        dp.Plot(
            eunis_positive,
            caption=f"Verdeling van {selected_headers.has_species.value_counts()[True]} positive headers over EUNIS typen.",
        ),
        dp.Plot(
            eunis_negative,
            caption=f"Verdeling van van {selected_headers.has_species.value_counts()[False]} negatieve headers over EUNIS typen.",
        ),
        f"\n\nCreated {datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')} by {os.environ.get('username')} using Altair and DataPane.",
    )
    app.save(f"./inspection_{slugify(identifier)}_{selected_headers.loc[selected_headers.has_species].shape[0]:07}_{structuurtype}.html")
    print('done!\n')


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("id", help="species identifier in Latin", type=str, nargs="+")
    parser.add_argument("search_radius", help="radius around positive plots in km", type=int, default=25)
    parser.add_argument("--minimum", help="minimum positive plots", type=int, default=1)
    args = parser.parse_args()

    plots = DorenPlots(source_data=SOURCE_DATA, prepared_data=PREPARED_DATA)

    for species in args.id:
        for structuur in [
            "moeras",
            "nat loofbos",
            "stromend water",
            "droog struweel",
            "nat dwergstruweel",
            "zout",
            "droog dwergstruweel",
            "droog grasland",
            "droog loofbos",
            "nat grasland",
            "water",
        ]:
            try:
                build_app(plots=plots, identifier=species, structuurtype=structuur,
                          minimum=args.minimum, search_radius=args.search_radius)
            except (ValueError, KeyError) as e:
                print(e)
                continue


