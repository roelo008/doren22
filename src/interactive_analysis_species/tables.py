import pathlib
import sys

import altair as alt
import datapane as dp
import pandas as pd

sys.path.append(str(pathlib.Path(__file__).parents[1]))

try:
    from preparation.settings import FILTERS, SOURCE_DATA

    import shared_specs as specs
    from header_queries import DorenPlots
except (ImportError, ModuleNotFoundError):
    import src.interactive_analysis_species.shared_specs as specs
    from src.interactive_analysis_species.header_queries import DorenPlots
    from src.preparation.settings import FILTERS, SOURCE_DATA

alt.data_transformers.enable("json")


def preparation_menu(doren: DorenPlots, buffer_distance, structuurtype) -> dp.Table:
    """
    Produce table in Datapane
    :param headers:
    :return:
    """

    return dp.Table(
        pd.DataFrame(
            {
                "EVA headers source": doren.preparation_menu["source_data"][
                    "eva_headers"
                ],
                "# headers before filtering": f'{doren.preparation_menu["n_starting"]:,}',
                # "EVA species": doren.preparation_menu["source_data"]["eva_species"],
                # "AOI": doren.preparation_menu["source_data"]["aoi"],
                "years": f'{doren.preparation_menu["filters"]["earliest_year"]} - {doren.preparation_menu["filters"]["latest_year"]}',
                "max elevation [m above MSL]": doren.preparation_menu["filters"][
                    "maximum_elevation"
                ],
                "NDep averaging period": f'{doren.preparation_menu["ndep_averaging_periode"]} year',
                # "nh3_data_source_forested": doren.preparation_menu["nh3_source"][
                #     "f"
                # ],
                # "nh3_data_source_grass": doren.preparation_menu["nh3_source"][
                #     "v"
                # ],
                # "nox_data_source_forested": doren.preparation_menu["nox_source"][
                #     "f"
                # ],
                # "nox_data_source_grass": doren.preparation_menu["nox_source"][
                #     "v"
                # ],
                "# headers after filtering": f"{doren.header_gdf.shape[0]:,}",
                "# positive headers": f"{doren.header_gdf.has_species.value_counts()[True]:,}",
                f"# positive headers with {structuurtype}": f'{doren.header_gdf.loc[(doren.header_gdf["has_species"]) & (doren.header_gdf["has_structuurtype"])].shape[0]:,}',
                "# negative headers": f"{doren.header_gdf.has_species.value_counts()[False]:,}",
                f"# negative headers with {structuurtype}": f'{doren.header_gdf.loc[(~doren.header_gdf["has_species"])& (doren.header_gdf["has_structuurtype"])].shape[0]:,}',
                f"# negative headers with {structuurtype} and within {buffer_distance}m": f"{doren.header_gdf.loc[(~doren.header_gdf.has_species) & (doren.header_gdf.has_structuurtype) & (doren.header_gdf.within_buffer), :, ].shape[0]:,}",
            },
            index=[""],
        ).T
    )
