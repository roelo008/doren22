import os
import pathlib
import sys
import warnings

from matplotlib_venn import venn3, venn3_circles
from matplotlib_venn._venn3 import compute_venn3_colors
import pandas as pd
from matplotlib import pyplot as plt

try:
    sys.path.append(str(pathlib.Path(__file__).parents[1]))
except NameError:
    pass

try:
    from preparation.header_management_functions import (
        slugify,
        species_name_translator,
        habitat_code_to_omschrijving,
    )
    from preparation.settings import FILTERS, PREPARED_DATA, SOURCE_DATA
except (ImportError, ModuleNotFoundError) as e:
    from src.preparation.header_management_functions import (
        slugify,
        species_name_translator,
        habitat_code_to_omschrijving,
    )
    from src.preparation.settings import FILTERS, PREPARED_DATA, SOURCE_DATA

CATEGORIES = ["doren19_kwalificerend", "doren19_verdringend", "RJ_karakt"]

soortenlijsten = (
    pd.read_excel(
        r"c:\Users\roelo008\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP06_soorenselectie_habitat\Soortenlijsten_def (punt6).xlsx",
        sheet_name=r"Alle lijsten (RL, RJ, T, V)",
        usecols=["nederlandse_naam", "Habitattype", "lijst_type", "is_rode_lijst"],
    )
    .rename(
        columns={
            "nederlandse_naam": "identifier",
            "Habitattype": "habtype",
            "lijst_type": "lijst",
        }
    )
    .dropna(subset=["identifier"])
)
red_list = set(
    pd.read_excel(
        SOURCE_DATA["doren_master_excel"],
        sheet_name="soorten_RL",
        usecols=["wetenschappelijke_naam", "nederlandse_naam", "rode_lijst"],
    )
    .query('rode_lijst == "j"')
    .nederlandse_naam
)



habitattype = 'H2110'

def build_venn(soortenlijsten, red_list, habitattype):
    """
    Build Venn diagram of species list for a Habitattype
    Parameters
    ----------
    habitattype

    Returns
    -------

    """

    # Sets of species
    s1 = set(
        soortenlijsten.loc[
            (soortenlijsten.habtype == habitattype)
            & (soortenlijsten.lijst == CATEGORIES[0]),
            "identifier",
        ]
    )
    s2 = set(
        soortenlijsten.loc[
            (soortenlijsten.habtype == habitattype)
            & (soortenlijsten.lijst == CATEGORIES[1]),
            "identifier",
        ]
    )
    s3 = set(
        soortenlijsten.loc[
            (soortenlijsten.habtype == habitattype)
            & (soortenlijsten.lijst == CATEGORIES[2]),
            "identifier",
        ]
    )

    if all([len(x) == 0 for x in [s1, s2, s3]]):
        print(f"empty sets for {habitattype}")
        return None

    # Seven portions of 3-circle Venn diagram
    p1 = s1.difference(s2).difference(s3)  # A_only  labelID=100
    p2 = s2.difference(s1).difference(s3)  # B only  labelID=010
    p3 = s1.intersection(s2).difference(s3)  # A and B not C  labelID=110
    p4 = s3.difference(s2).difference(s1)  # C only  labelID=001
    p5 = s1.intersection(s3).difference(s2)  # A and C not B  labelID=101
    p6 = s2.intersection(s3).difference(s1)  # B and C not A  labelID=011
    p7 = s1.intersection(s2).intersection(s3)  # s1_and_s2_and_s3   labelID=111
    subsets = (len(p1), len(p2), len(p3), len(p4), len(p5), len(p6), len(p7))

    # Labels are nr of species and nr of red-list species
    labels = {
        "100": f"{len(p1)}[{len(p1.intersection(red_list))}]",
        "010": f"{len(p2)}[{len(p2.intersection(red_list))}]",
        "110": f"{len(p3)}[{len(p3.intersection(red_list))}]",
        "001": f"{len(p4)}[{len(p4.intersection(red_list))}]",
        "101": f"{len(p5)}[{len(p5.intersection(red_list))}]",
        "011": f"{len(p6)}[{len(p6.intersection(red_list))}]",
        "111": f"{len(p7)}[{len(p7.intersection(red_list))}]",
    }

    plt.figure(figsize=(2,2))
    # plt.subplot(1, 1, 1)

    # Build the venn diagram
    venn = venn3(
        subsets=subsets,
        set_labels=CATEGORIES,
    )

    # Adjust label colors and size for the 3 groups
    base_colors = compute_venn3_colors(set_colors=('r', 'g', 'b'))
    for label_id, color_idx in dict(zip(["A", "B", "C"], [0, 1, 3])).items():
        venn.get_label_by_id(label_id).set_size(8)
        venn.get_label_by_id(label_id).set_color(base_colors[color_idx])
        venn.get_label_by_id(label_id).set_alpha(0.4)

    # Disable label for a group is its area equals minimal area threshold
    for label_id, area in zip(['A', 'B', 'C'], venn.areas[:3]):
        if area == 1e-6:
            venn.get_label_by_id(label_id).set_visible(False)

    # Overwrite portion labels to show nr of species and red-list species bracketed
    for k, v in labels.items():
        try:
            venn.get_label_by_id(k).set_text(v)
            venn.get_label_by_id(k).set_color("black")
            venn.get_label_by_id(k).set_size(6)
            venn.get_label_by_id(k).set_alpha(0.4)
        except AttributeError:
            pass
    venn.hide_zeroes()

    c = venn3_circles(subsets=subsets, linestyle="dashed", linewidth=0.5)
    # plt.title(f"Verdeling voor {habitattype}-{habitat_code_to_omschrijving(habitattype)}")
    return plt.gcf()


def build_table(soortenlijsten: pd.DataFrame, red_list, habitattype):
    """

    Parameters
    ----------
    habitattype

    Returns
    -------

    """
    # Data table

    piv = pd.pivot_table(
        data=soortenlijsten.loc[soortenlijsten.habtype == habitattype, :],
        index="identifier",
        columns="lijst",
        values="habtype",
        aggfunc="count",
    ).fillna(
        0
    )  # map({1: True, 0: False})
    if piv.empty:
        return None

    for categorie in CATEGORIES:
        if categorie not in list(piv):
            piv.loc[:, categorie] = 0

    piv['latin'] = [species_name_translator(x, is_what='nederlandse_naam', return_as='wetenschappelijke_naam', strict=False) for x in piv.index]
    piv["rode_lijst"] = piv.index.isin(red_list)
    return piv.loc[:, ['latin', 'rode_lijst'] + CATEGORIES]


habitattypen = [
    "H1310-A",
    "H1310-B",
    "H1320",
    "H1330-A",
    "H1330-B",
    "H2110",
    "H2120",
    "H2130-A",
    "H2130-B",
    "H2130-C",
    "H2140-A",
    "H2140-B",
    "H2150",
    "H2160",
    "H2170",
    "H2180-A",
    "H2180-B",
    "H2180-C",
    "H2190-A",
    "H2190-B",
    "H2190-C",
    "H2190-D",
    "H2310",
    "H2320",
    "H2330",
    "H3110",
    "H3130",
    "H3140",
    "H3150",
    "H3160",
    "H3260-A",
    "H3260-B",
    "H3270",
    "H4010-A",
    "H4010-B",
    "H4030",
    "H5130",
    "H6110",
    "H6120",
    "H6130",
    "H6210",
    "H6230",
    "H6410",
    "H6430-A",
    "H6430-B",
    "H6430-C",
    "H6510-A",
    "H6510-B",
    "H7110-A",
    "H7110-B",
    "H7120",
    "H7140-A",
    "H7140-B",
    "H7150",
    "H7210",
    "H7220",
    "H7230",
    "H9110",
    "H9120",
    "H9160-A",
    "H9160-B",
    "H9190",
    "H91D0",
    "H91E0-A",
    "H91E0-B",
    "H91E0-C",
    "H91F0",
]
# for hab in habitattypen:
#     print(hab)
#     build_venn(hab)
