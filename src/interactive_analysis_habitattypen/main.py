import pathlib
import sys

from matplotlib import pyplot as plt
import altair as alt
import datapane as dp

try:
    sys.path.append(str(pathlib.Path(__file__).parents[1]))
except NameError:
    pass

try:
    from venn import build_venn, habitattypen, build_table, soortenlijsten, red_list
    from preparation.header_management_functions import (
        slugify,
        species_name_translator,
        habitat_code_to_omschrijving,
    )
except ModuleNotFoundError:
    from src.interactive_analysis_habitattypen.venn import (
        build_venn,
        habitattypen,
        build_table,
        soortenlijsten,
        red_list,
    )
    from src.preparation.header_management_functions import (
        slugify,
        species_name_translator,
        habitat_code_to_omschrijving,
    )

contents = {}
for hab in habitattypen:
    d = {
        "venn": build_venn(
            soortenlijsten=soortenlijsten, red_list=red_list, habitattype=hab
        ),
        "table": build_table(
            soortenlijsten=soortenlijsten, red_list=red_list, habitattype=hab
        ),
    }
    if d["venn"] is not None and d["table"] is not None:
        contents[hab] = d
    plt.close()

alt.data_transformers.enable("json")

app = dp.App(
    "# DOREN 2023 - analyse van soortenlijsten voor Habitattypen.",
    f'## H1320-{habitat_code_to_omschrijving("H1320")}',
    dp.Plot(contents["H1320"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H1320"]["table"], caption="Details soortenlijsten."),
    f'## H2110-{habitat_code_to_omschrijving("H2110")}',
    dp.Plot(contents["H2110"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2110"]["table"], caption="Details soortenlijsten."),
    f'## H2120-{habitat_code_to_omschrijving("H2120")}',
    dp.Plot(contents["H2120"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2120"]["table"], caption="Details soortenlijsten."),
    f'## H2150-{habitat_code_to_omschrijving("H2150")}',
    dp.Plot(contents["H2150"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2150"]["table"], caption="Details soortenlijsten."),
    f'## H2160-{habitat_code_to_omschrijving("H2160")}',
    dp.Plot(contents["H2160"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2160"]["table"], caption="Details soortenlijsten."),
    f'## H2170-{habitat_code_to_omschrijving("H2170")}',
    dp.Plot(contents["H2170"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2170"]["table"], caption="Details soortenlijsten."),
    f'## H2310-{habitat_code_to_omschrijving("H2310")}',
    dp.Plot(contents["H2310"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2310"]["table"], caption="Details soortenlijsten."),
    f'## H2320-{habitat_code_to_omschrijving("H2320")}',
    dp.Plot(contents["H2320"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2320"]["table"], caption="Details soortenlijsten."),
    f'## H2330-{habitat_code_to_omschrijving("H2330")}',
    dp.Plot(contents["H2330"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H2330"]["table"], caption="Details soortenlijsten."),
    f'## H3110-{habitat_code_to_omschrijving("H3110")}',
    dp.Plot(contents["H3110"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3110"]["table"], caption="Details soortenlijsten."),
    f'## H3130-{habitat_code_to_omschrijving("H3130")}',
    dp.Plot(contents["H3130"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3130"]["table"], caption="Details soortenlijsten."),
    f'## H3140-{habitat_code_to_omschrijving("H3140")}',
    dp.Plot(contents["H3140"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3140"]["table"], caption="Details soortenlijsten."),
    f'## H3150-{habitat_code_to_omschrijving("H3150")}',
    dp.Plot(contents["H3150"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3150"]["table"], caption="Details soortenlijsten."),
    f'## H3160-{habitat_code_to_omschrijving("H3160")}',
    dp.Plot(contents["H3160"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3160"]["table"], caption="Details soortenlijsten."),
    f'## H3270-{habitat_code_to_omschrijving("H3270")}',
    dp.Plot(contents["H3270"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H3270"]["table"], caption="Details soortenlijsten."),
    f'## H4030-{habitat_code_to_omschrijving("H4030")}',
    dp.Plot(contents["H4030"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H4030"]["table"], caption="Details soortenlijsten."),
    f'## H5130-{habitat_code_to_omschrijving("H5130")}',
    dp.Plot(contents["H5130"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H5130"]["table"], caption="Details soortenlijsten."),
    f'## H6110-{habitat_code_to_omschrijving("H6110")}',
    dp.Plot(contents["H6110"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6110"]["table"], caption="Details soortenlijsten."),
    f'## H6120-{habitat_code_to_omschrijving("H6120")}',
    dp.Plot(contents["H6120"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6120"]["table"], caption="Details soortenlijsten."),
    f'## H6130-{habitat_code_to_omschrijving("H6130")}',
    dp.Plot(contents["H6130"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6130"]["table"], caption="Details soortenlijsten."),
    f'## H6210-{habitat_code_to_omschrijving("H6210")}',
    dp.Plot(contents["H6210"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6210"]["table"], caption="Details soortenlijsten."),
    f'## H6230-{habitat_code_to_omschrijving("H6230")}',
    dp.Plot(contents["H6230"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6230"]["table"], caption="Details soortenlijsten."),
    f'## H6410-{habitat_code_to_omschrijving("H6410")}',
    dp.Plot(contents["H6410"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H6410"]["table"], caption="Details soortenlijsten."),
    f'## H7120-{habitat_code_to_omschrijving("H7120")}',
    dp.Plot(contents["H7120"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H7120"]["table"], caption="Details soortenlijsten."),
    f'## H7150-{habitat_code_to_omschrijving("H7150")}',
    dp.Plot(contents["H7150"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H7150"]["table"], caption="Details soortenlijsten."),
    f'## H7210-{habitat_code_to_omschrijving("H7210")}',
    dp.Plot(contents["H7210"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H7210"]["table"], caption="Details soortenlijsten."),
    f'## H7220-{habitat_code_to_omschrijving("H7220")}',
    dp.Plot(contents["H7220"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H7220"]["table"], caption="Details soortenlijsten."),
    f'## H7230-{habitat_code_to_omschrijving("H7230")}',
    dp.Plot(contents["H7230"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H7230"]["table"], caption="Details soortenlijsten."),
    f'## H9110-{habitat_code_to_omschrijving("H9110")}',
    dp.Plot(contents["H9110"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H9110"]["table"], caption="Details soortenlijsten."),
    f'## H9120-{habitat_code_to_omschrijving("H9120")}',
    dp.Plot(contents["H9120"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H9120"]["table"], caption="Details soortenlijsten."),
    f'## H9190-{habitat_code_to_omschrijving("H9190")}',
    dp.Plot(contents["H9190"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H9190"]["table"], caption="Details soortenlijsten."),
    f'## H91D0-{habitat_code_to_omschrijving("H91D0")}',
    dp.Plot(contents["H91D0"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H91D0"]["table"], caption="Details soortenlijsten."),
    f'## H91F0-{habitat_code_to_omschrijving("H91F0")}',
    dp.Plot(contents["H91F0"]["venn"], caption="Overlap soortenlijsten."),
    dp.DataTable(contents["H91F0"]["table"], caption="Details soortenlijsten."),
)
app.save(r"c:/apps/z_temp/soortenlijsten_habtypen.html")

# template = r"c:\apps\z_temp\template.txt"
# destination = r"c:\apps\z_temp\template_2.txt"
# with open(destination, "w") as dest:
#     for hab, _ in contents.items():
#         with open(template, "r") as source:
#             lines = source.readlines()
#             for line in lines:
#                 dest.write(line.replace("XX", hab))
#         dest.write("\n")
#         dest.write("\n")
#
