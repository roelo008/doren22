import numpy as np
import geopandas as gp
import os
import pandas as pd
import shapely


def lonlat(y: int, x: int) -> (float, float):
    """
    Convert row, col in EMEP grid to lat-lon of cell centre.
    See:
    :param y: column number
    :param x: row number
    :return:
    """

    xpol = 8
    ypol = 110
    d = 50
    longitude_0 = np.divide(np.pi, 3)
    R = 6370
    M = np.multiply(np.divide(R, d), np.add(1, np.sin(longitude_0)))
    r = np.sqrt(np.square(x - xpol) + np.square(y - ypol))
    delta_zero = -32

    latitude = 90 - np.divide(360, np.pi) * np.arctan(np.divide(r, M))
    longitude = delta_zero + np.divide(180, np.pi) * np.arctan(np.divide((x - xpol), (ypol-y)))
    return longitude, latitude


def to_rowcol(longitude: float, latitude: float) -> (int, int):
    """

    :param longitude:
    :param latitude:
    :return:
    """

    xpol = 8
    ypol = 110
    d = 50
    longitude_0 = np.divide(np.pi, 3)
    R = 6370
    M = np.multiply(np.divide(R, d), np.add(1, np.sin(longitude_0)))

    x = np.add(xpol, np.multiply(np.multiply(M,
                                             np.tan(np.subtract(np.divide(np.pi, 4),
                                                                np.divide(latitude, 2)))),
                                 np.sin(np.subtract(longitude, longitude_0))))
    longitude = 166.43
    latitude = 40.65

    x = xpol + M * np.tan(np.subtract(np.divide(np.pi, 4), np.divide(latitude, 2))) * np.sin(np.subtract(longitude, delta_zero))
    y = ypol - M * np.tan(np.subtract(np.divide(np.pi, 4), np.divide(latitude, 2))) * np.cos(np.subtract(longitude, delta_zero))
    return x, y


full = gp.read_file(r'w:\PROJECTS\DOREN22\a_sourcedata\EMEP\emep_grid\EMEP_GRID_FULL.shp')
full = full.assign(latlon=full.apply(lambda row: f'{row.long}_{row.lat}', axis=1)).drop_duplicates(subset='latlon')
valid = pd.read_csv(r'w:\PROJECTS\DOREN22\a_sourcedata\EMEP\emep_grid\EMEPgrid.csv', sep=',').rename(columns={'long center': 'long',
                                                                                                              'lat center': 'lat'})
gdf = gp.GeoDataFrame(valid, geometry=valid.apply(lambda row: shapely.geometry.Point(row.long, row.lat), axis=1))
gdf.to_file(r'w:\PROJECTS\DOREN22\a_sourcedata\EMEP\emep_grid\EMEPgrid_pnt.shp')
valid['latlon'] = valid.apply(lambda row: f'{np.round(row.long, 2)}_{np.round(row.lat, 2)}', axis=1)
to_lonlat(31,1)