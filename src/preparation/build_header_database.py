import datetime
import os

try:
    import header_management_functions as prep
    from settings import BASE_OUT_DIRECTORY, EUNIS_OVERWRITE, FILTERS, SOURCE_DATA
except ModuleNotFoundError as e:
    from src.preparation import header_management_functions as prep
    from src.preparation.settings import (
        BASE_OUT_DIRECTORY,
        EUNIS_OVERWRITE,
        FILTERS,
        SOURCE_DATA,
    )


def buid_header_database(sample: bool, basename: str, ndep_averaging_period: int,
                         as_doren1=False):
    """
    Build *.csv and *.shp files with EVA headers based on selections and with additional columns
    :param sample: use sample of the dataset, for testing purposes
    :param basename: file basena,e
    :return:
    """

    report = f'Starting {datetime.datetime.now().strftime("%d %b %Y %H:%M:%S")}'
    print(report)

    ts = datetime.datetime.now().strftime("%Y%m%d-%H%M")

    # Output directory
    out_dir = os.path.join(BASE_OUT_DIRECTORY, f'{basename}_{ts}')
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    if as_doren1:
        raise NotImplementedError('Dit is niet goed geimplementeerd, dd 20230406')
        df, report = prep.get_doren1_headers(msg=report)
    else:
        df, report, n_starting = prep.get_headers(msg=report)

    if sample:
        df, report = prep.sample(df=df, msg=report, n=1000)

    df, report = prep.add_xy_3035(df, msg=report)
    if not as_doren1:
        df, report = prep.filter_to_years(df, msg=report)
        df, report = prep.filter_to_species_lists(df, msg=report)
        df, report = prep.filter_to_aoi(df, msg=report)
        df, report = prep.filter_to_elevation(df, msg=report)
    df, report = prep.add_country(df, msg=report)
    df, report = prep.add_soil_type(df, msg=report)
    df, report = prep.add_simple_soil_type(df, msg=report)
    df, report = prep.add_yearly_precipitation(df, msg=report)
    df, report = prep.add_yearly_temperature(df, msg=report)
    df, report = prep.merge_old_new_eunis(df, msg=report)
    df, report = prep.composite_eunis_to_single(df, msg=report)
    df, report = prep.overwrite_eunis(gdf=df, msg=report)
    df, report = prep.add_structuurtype(gdf=df, msg=report, out_dir=out_dir,
                                        timestamp=ts)
    df, report = prep.add_roughness(gdf=df, msg=report)
    df, report = prep.add_ndep_max(
        df,
        msg=report,
        ndep_src=SOURCE_DATA["nh3_averaged"][f"{ndep_averaging_period}_yr"],
        target_name="nh3_mg_m2",
    )
    df, report = prep.add_ndep_max(
        df,
        msg=report,
        ndep_src=SOURCE_DATA["nox_averaged"][f"{ndep_averaging_period}_yr"],
        target_name="nox_mg_m2",
    )
    df, report = prep.add_n_totals(df, msg=report)

    # Write headers to CSV
    df.drop("xy_3035", axis=1).to_csv(
        os.path.join(out_dir, f"{ts}_{basename}.csv"), sep=",", index=True
    )

    # Write headers to SHP
    df.to_file(os.path.join(out_dir, f"{ts}_{basename}.shp"))

    # Write report as *.txt
    with open(os.path.join(out_dir, f"{ts}_{basename}.txt"), "w") as f:
        f.write(report)

    # Write settings to json
    prep.write_specs_to_file(
        out_dir,
        f"{ts}_{basename}",
        source_data={
            k: v
            for k, v in SOURCE_DATA.items()
            if k not in ["nh3_averaged", "nox_averaged"]
        },
        filters=FILTERS,
        n_starting=n_starting,
        ndep_averaging_periode=ndep_averaging_period,
        nh3_source=SOURCE_DATA["nh3_averaged"][f"{ndep_averaging_period}_yr"],
        nox_source=SOURCE_DATA["nox_averaged"][f"{ndep_averaging_period}_yr"],
    )

    # Write species list to file
    species_list_summary = prep.species_list_(df=df)
    species_list_summary.to_csv(
        os.path.join(out_dir, f'{ts}_{basename}_species_list_summary.csv'),
        index_label='matched_concept', sep=','
    )

    # OPTIONAL TO DO: copy SOURCE_DATA["eva_specieS"] and write back to file with only selected headers.

    print(
        f'Done @ {datetime.datetime.now().strftime("%d %b %Y %H:%M:%S")}. See results: {BASE_OUT_DIRECTORY}\\{basename}!'
    )


def build_headers_total_ndep(sample: bool, basename: str):
    """
    Build header database with total summed NDep from 1880 to year of survey!
    Parameters
    ----------
    sample
    basename

    Returns
    -------

    """

    report = f'Starting {datetime.datetime.now().strftime("%d %b %Y %H:%M:%S")}'
    print(report)

    ts = datetime.datetime.now().strftime("%Y%m%d-%H%M")

    # Output directory
    out_dir = os.path.join(BASE_OUT_DIRECTORY, f'{basename}_{ts}')
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    df, report, n_starting = prep.get_headers(msg=report)

    if sample:
        df, report = prep.sample(df=df, msg=report, n=1000)

    df, report = prep.add_xy_3035(df, msg=report)
    df, report = prep.filter_to_years(df, msg=report)
    df, report = prep.filter_to_species_lists(df, msg=report)
    df, report = prep.filter_to_aoi(df, msg=report)
    df, report = prep.filter_to_elevation(df, msg=report)
    df, report = prep.add_country(df, msg=report)
    df, report = prep.add_soil_type(df, msg=report)
    df, report = prep.add_simple_soil_type(df, msg=report)
    df, report = prep.add_yearly_precipitation(df, msg=report)
    df, report = prep.add_yearly_temperature(df, msg=report)
    df, report = prep.merge_old_new_eunis(df, msg=report)
    df, report = prep.composite_eunis_to_single(df, msg=report)
    df, report = prep.overwrite_eunis(gdf=df, msg=report)
    df, report = prep.add_structuurtype(gdf=df, msg=report, out_dir=out_dir,
                                        timestamp=ts)
    df, report = prep.add_roughness(gdf=df, msg=report)
    df, report = prep.add_ndep_yearly(
        gdf=df,
        msg=report,
        ndep_src=SOURCE_DATA["nh3_averaged"]['yearly'],
        target_name="nh3_mg_m2",
        t_delta=9999,
        aggfunc='sum',
        drop_na=True,
    )
    df, report = prep.add_ndep_yearly(
        gdf=df,
        msg=report,
        ndep_src=SOURCE_DATA["nox_averaged"]["yearly"],
        target_name="nox_mg_m2",
        t_delta=9999,
        aggfunc='sum',
        drop_na=True,
    )
    df, report = prep.add_n_totals(df, msg=report)

    # Write headers to CSV
    df.drop("xy_3035", axis=1).to_csv(
        os.path.join(out_dir, f"{ts}_{basename}.csv"), sep=",", index=True
    )

    # Write headers to SHP

    # Write report as *.txt
    with open(os.path.join(out_dir, f"{ts}_{basename}.txt"), "w") as f:
        f.write(report)

    # Write settings to json

    # Write species list to file
    species_list_summary = prep.species_list_(df=df)
    species_list_summary.to_csv(
        os.path.join(out_dir, f'{ts}_{basename}_species_list_summary.csv'),
        index_label='matched_concept', sep=','
    )

    print(
        f'Done @ {datetime.datetime.now().strftime("%d %b %Y %H:%M:%S")}. See results: {BASE_OUT_DIRECTORY}\\{basename}!'
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "ndep_period",
        help="NDeposition averaging period",
        type=int,
        default=10,
        choices=[5, 10, 20, 30, 2019],
    )
    parser.add_argument(
        "--basename", help="file basename", type=str, default="eva_headers"
    )
    parser.add_argument(
        "--sample", help="sample to 1000 records. For testing", action="store_true"
    )
    parser.add_argument("--as_doren1", help='Use 513.850 headers from DOREN 1',
                        action='store_true')
    args = parser.parse_args()

    buid_header_database(
        sample=args.sample,
        basename=args.basename,
        ndep_averaging_period=args.ndep_period,
        as_doren1=args.as_doren1,
    )
