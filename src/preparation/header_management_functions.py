import re
import os
import unicodedata
import geopandas as gp
import pandas as pd
import pyproj
from shapely.geometry import Point
import rasterstats
import rasterio as rio
import numpy as np
import sys
import pathlib
import json
from typing import Any

sys.path.append(str(pathlib.Path(__file__).parents[0]))
try:
    from settings import SOURCE_DATA, FILTERS, EUNIS_OVERWRITE, BASE_OUT_DIRECTORY
except ModuleNotFoundError:
    from src.preparation.settings import (
        SOURCE_DATA,
        FILTERS,
        EUNIS_OVERWRITE,
        BASE_OUT_DIRECTORY,
    )


def sample(
    df: pd.DataFrame,
    msg: str,
    n: int,
) -> (pd.DataFrame, str):
    """
    Return sample of the dataframe
    :param msg:
    :param df:
    :param n:
    :return:
    """

    update = f"Random sampling to {n} records."
    print(update)
    return df.sample(n), f"{msg}\n{update}"


def get_headers(
    msg: str,
    headers_src: str = SOURCE_DATA["eva_headers"],
) -> (pd.DataFrame, str):
    """
    Read EVA header file, using selected columns and dropping rows with NA in selected columns
    :param headers_src: source file
    :return: pandas dataframe
    """

    df = (
        pd.read_csv(
            headers_src,
            sep=",",
            header=1,
            comment="#",
            usecols=[
                "plot_id",
                # "tv2_releve_nr",
                "longitude",
                "latitude",
                "year",
                "dataset",
                "eunis_old",
                "eunis_new",
            ],
            names=[
                "plot_id",
                "tv2_releve_nr",
                "country",
                "cover_abundance_scale",
                "year",
                "releve_area_m2",
                "altitude_m",
                "aspect_deg",
                "slope_deg",
                "eunis_old",
                "eunis_new",
                "longitude",
                "latitude",
                "location_uncertainty_m",
                "dataset",
            ],
            na_values={"eunis_old": "?", "eunis_new": ["?", "I"]},
        )
        .drop_duplicates("plot_id")
        .set_index("plot_id", drop=True)
        .dropna(axis=0, how="any", subset=["latitude", "longitude", "year"])
        .dropna(axis=0, how="all", subset=["eunis_old", "eunis_new"])
        .astype({"year": int})
    )
    update = f"Reading headers from file {headers_src}, {df.shape[0]:,} remaining after dropping NAs in latitude, longitude and date fields and (eunis_old AND eunis_new)."
    print(update)
    return (df, f"{msg}\n{update}", df.shape[0])


def get_doren1_headers(
    msg: str,
    headers_src: str = SOURCE_DATA["eva_headers"],
) -> (pd.DataFrame, str):
    """
    Filter headers to the 513.850 headers as used in DOREN1
    """
    doren1_ids = pd.read_csv(
        r"W:\PROJECTS\Doren19\b_compiled_data\DOREN_20210113\pg_input\.Plot_Covars.csv",
        sep=",",
        index_col="plot_obs_id",
    ).index
    headers = pd.read_csv(
        headers_src,
        sep=",",
        header=1,
        comment="#",
        usecols=[
            "plot_id",
            # "tv2_releve_nr",
            "longitude",
            "latitude",
            "year",
            "dataset",
            "eunis_old",
            "eunis_new",
        ],
        names=[
            "plot_id",
            "tv2_releve_nr",
            "country",
            "cover_abundance_scale",
            "year",
            "releve_area_m2",
            "altitude_m",
            "aspect_deg",
            "slope_deg",
            "eunis_old",
            "eunis_new",
            "longitude",
            "latitude",
            "location_uncertainty_m",
            "dataset",
        ],
        index_col="plot_id",
    )
    df = headers.loc[doren1_ids, :]
    update = f"Reader 513.850 headers from DOREN1."
    print(update)
    return (df, f"{msg}\n{update}")


def filter_to_years(
    df: pd.DataFrame,
    msg: str,
    earliest_year=FILTERS["earliest_year"],
) -> (pd.DataFrame, str):
    """
    Restrict header data frame to plots not older than a year
    :param df:
    :param earliest_year:
    :return:
    """
    pre = df.shape[0]
    df = df.query(f"year >= {earliest_year}")
    update = f"Filtering {pre:,} headers for not older than {earliest_year}, {df.shape[0]:,} remaining."
    print(update)
    return (df, f"{msg}\n{update}")


def filter_to_species_lists(
    df: pd.DataFrame,
    msg: str,
    species_src=SOURCE_DATA["eva_species"],
) -> (pd.DataFrame, str):
    """
    Restrict the dataframe to plots which have a corresponding species inventory
    :param df:
    :return:
    """

    pre = df.shape[0]

    # Drop plots w/o a matching species inventory
    df = df.loc[
        df.index.intersection(
            set(
                pd.read_csv(
                    species_src,
                    sep="\t",
                    usecols=["PlotObservationID"],
                ).PlotObservationID
            )
        )
    ]
    update = f"Filtering {pre:,} headers for having a valid species list in {species_src}, {df.shape[0]:,} remaining."
    print(update)
    return (df, f"{msg}\n{update}")


def filter_to_aoi(
    gdf: gp.GeoDataFrame,
    msg: str,
    aoi_src=SOURCE_DATA["aoi"],
) -> (gp.GeoDataFrame, str):
    """
    Filter EVA plots to within AOI
    :param gdf: EVA gdf
    :param aoi_src: path to aoi
    :return: gdf
    """

    pre = gdf.shape[0]

    out = gdf.loc[
        gp.sjoin(
            left_df=gdf, right_df=gp.read_file(aoi_src), how="inner", predicate="within"
        ).index
    ]
    update = f"Filtering {pre:,} headers to AOI {aoi_src}, {out.shape[0]:,} remaining."
    print(update)
    return (out, f"{msg}\n{update}")


def filter_to_elevation(
    gdf: gp.GeoDataFrame,
    msg: str,
    elevation_src=SOURCE_DATA["elevation"],
    max_elevation=FILTERS["maximum_elevation"],
) -> (gp.GeoDataFrame, str):
    dem = rio.open(elevation_src)
    dem_array = dem.read(1)
    pre = gdf.shape[0]
    gdf["elevation"] = rasterstats.point_query(
        vectors=gdf,
        raster=dem_array,
        interpolate="nearest",
        nodata=dem.nodata,
        affine=dem.transform,
    )
    gdf = gdf.loc[gdf.elevation <= max_elevation, :]
    update = f"Filtering {pre:,} headers for elevation below {max_elevation}m based on {elevation_src}, with {gdf.shape[0]:,} headers remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def add_xy_3035(df: pd.DataFrame, msg: str) -> (gp.GeoDataFrame, str):
    """
    Add EPSG3035 easting northing coordinates as shapely point
    :param df:
    :return:
    """

    # Add epsg3035 coordinates
    eastings, northings = pyproj.Transformer.from_crs(
        crs_from="epsg:4326", crs_to="epsg:3035", always_xy=True
    ).transform(df.longitude, df.latitude)
    df["easting"] = eastings
    df["northing"] = northings
    df["xy_3035"] = [Point(x, y) for (x, y) in zip(eastings, northings)]

    # Return
    update = f"Adding EPSG3035 coordinates for {df.shape[0]:,} headers"
    print(update)
    return (
        gp.GeoDataFrame(df, geometry="xy_3035", crs="epsg:3035"),
        f"{msg}\n{update}",
    )


def add_soil_type(
    gdf: gp.GeoDataFrame,
    msg: str,
    soil_src=SOURCE_DATA["soil_map"],
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """
    Add soil type to plots
    :param gdf:
    :param soil_src:
    :return:
    """
    category_map = (
        gp.read_file(f"{os.path.splitext(soil_src)[0]}.tif.vat.dbf")
        .set_index("VALUE")
        .descriptio.to_dict()
    )
    category_map = {k: v for k, v in category_map.items() if v != "unknown"}

    pre = gdf.shape[0]

    raster = rio.open(soil_src)
    array = raster.read(1)

    gdf["soil_type"] = [
        category_map.get(i, np.nan)
        for i in rasterstats.point_query(
            vectors=gdf,
            raster=array,
            interpolate="nearest",
            nodata=raster.nodata,
            affine=raster.transform,
        )
    ]

    if drop_na:
        gdf = gdf.dropna(subset=["soil_type"])

    update = f"Adding soil type from {soil_src} to {pre:,} headers, with {gdf.shape[0]:,} headers remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def add_simple_soil_type(
    gdf: gp.GeoDataFrame,
    msg: str,
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """
    Add simplified soil type based on soil type
    :param gdf:
    :param msg:
    :return:
    """

    mapper = dict_from_file(
        src=SOURCE_DATA["simplified_soil_types"]["src"],
        key_column=SOURCE_DATA["simplified_soil_types"]["key"],
        value_column=SOURCE_DATA["simplified_soil_types"]["value"],
        sheet=SOURCE_DATA["simplified_soil_types"]["sheet"],
    )

    pre = gdf.shape[0]
    gdf["soil_type_simple"] = [mapper.get(i, np.nan) for i in gdf.soil_type]

    if drop_na:
        gdf = gdf.dropna(subset=["soil_type_simple"])

    update = f"Adding simplified soil type from {SOURCE_DATA['simplified_soil_types']['src']} to {pre:,} headers, with {gdf.shape[0]:,} headers remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def add_country(
    gdf: gp.GeoDataFrame,
    msg: str,
    country_src=SOURCE_DATA["country_map"],
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """
    Add country name to EVA header DF, based on coordinates
    :param df:
    :param drop_na: remove headers with NA for country
    :return:
    """

    # category_map = pd.read_csv(r'w:\PROJECTS\Doren19\a_brondata\covariables\countries\iso_country_codes.csv',
    #                            index_col='ISO3', sep=';', encoding='cp1252').Country_name.to_dict()

    countries = gp.read_file(country_src)
    out = (
        gdf.sjoin(
            countries.loc[:, ["geometry", "SOV_A3"]], how="left", predicate="within"
        )
        .rename(columns={"SOV_A3": "country"})
        .drop(labels="index_right", axis=1)
    )

    if drop_na:
        out = out.dropna(subset=["country"])

    update = f"Adding country information from {country_src} to {gdf.shape[0]:,} headers, with {out.shape[0]:,} headers remaining."
    print(update)
    return (out, f"{msg}\n{update}")


def add_yearly_temperature(
    gdf: gp.GeoDataFrame,
    msg: str,
    temp_src=SOURCE_DATA["temperature_directory"],
    temp_basename=SOURCE_DATA["temperature_basename"],
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """
    Read temperature value from geospatial raster corresponding to year of plot observation
    :param gdf:
    :param temp_src:
    :param temp_basename:
    :return:
    """

    pre = gdf.shape[0]
    for year in set(gdf.year):
        selected_plots = gdf.loc[gdf.year == year].index
        raster = rio.open(os.path.join(temp_src, f"{temp_basename}{year}.tif"))
        array = raster.read(1)
        gdf.loc[selected_plots, "temp_5yr"] = rasterstats.point_query(
            vectors=gdf.loc[selected_plots, :],
            raster=array,
            interpolate="bilinear",
            nodata=raster.nodata,
            affine=raster.transform,
        )

    if drop_na:
        gdf.dropna(subset=["temp_5yr"], inplace=True)

    update = f"Adding 5-yearly averaged temperature from {temp_src} to {pre:,} headers, with {gdf.shape[0]:,} headers remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def add_yearly_precipitation(
    gdf: gp.GeoDataFrame,
    msg: str,
    precip_src=SOURCE_DATA["precipitation_directory"],
    precip_basename=SOURCE_DATA["precipitation_basename"],
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """
    Add yearly precipitation value from geospatial raster corresponding to year of plot observation
    :param gdf:
    :param precip_src:
    :param precip_basename:
    :return:
    """

    pre = gdf.shape[0]
    for year in set(gdf.year):
        selected_plots = gdf.loc[gdf.year == year].index
        raster = rio.open(os.path.join(precip_src, f"{precip_basename}{year}.tif"))
        array = raster.read(1)
        gdf.loc[selected_plots, "precip_5yr"] = rasterstats.point_query(
            vectors=gdf.loc[selected_plots, :],
            raster=array,
            interpolate="bilinear",
            nodata=raster.nodata,
            affine=raster.transform,
        )
    if drop_na:
        gdf.dropna(subset=["precip_5yr"], inplace=True)
    update = f"Adding 5-yearly averaged precipitation from {precip_src} to {pre:,} headers, with {gdf.shape[0]:,} headers remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def read_ndep_file(src: str, headers_as_str: bool=True) -> pd.DataFrame:
    """
    Read a NDep file from Max Posch
    Parameters
    ----------
    src
    headers_as_int

    Returns
    -------

    """

    # Verify the file has 4 header lines starting with "!"
    with open(src, 'r') as f:
        head = [next(f) for _ in range(4)]
    assert all([l.startswith('!') for l in head]), f"{src} header is not formatted as expected."

    # Read the file and drop Lat Lon columns
    ndep = pd.read_csv(
        src,
        sep=',',
        skiprows=3,
        index_col=0,
        skip_blank_lines=True,
    ).drop(columns=['Lon', 'Lat'])

    # Sometimes there's a redundant "," in the file, creating an empty column
    ndep.drop(columns=[c for c in ndep.columns if c.startswith('Unnamed')], inplace=True)

    # Reset the index name
    ndep.index.name = 'plot_id'

    # Reformat the columns as requested
    if headers_as_str:
        ndep.columns = [f'y{i}' for i in ndep.columns]
    else:
        ndep.columns = [int(i) for i in ndep.columns]

    return ndep


def add_ndep_max(
    gdf: gp.GeoDataFrame,
    msg: str,
    target_name: str,
    ndep_src: dict,
    drop_na=True,
) -> (gp.GeoDataFrame, str):
    """

    :param gdf:
    :param msg:
    :param target_name:
    :param ndep_src:
    :param drop_na:
    :return:
    """

    # N-deposition is differentiated between hoog (f) and laag (v) vegetation.
    d = {"hoog": "f", "laag": "v"}

    for k, v in d.items():
        pre = gdf.loc[gdf["ruwheid"] == k].shape[0]

        # Read NDep data
        ndep = read_ndep_file(src=ndep_src[v], headers_as_str=True)

        # restrict NDep and GDF to common plots
        intersected_rows = ndep.index.intersection(gdf.loc[gdf["ruwheid"] == k].index)
        intersected_cols = ndep.columns.intersection(set([f"y{y}" for y in gdf.year]))
        ndep = ndep.loc[intersected_rows, intersected_cols]

        # Redesign NDep df to so that specific plot-year combinations can be extracted
        mapper = gdf.loc[intersected_rows].assign(
            year=[f"y{y}" for y in gdf.loc[intersected_rows, "year"]]
        )
        idx, cols = pd.factorize(mapper.year)
        gdf.loc[intersected_rows, target_name] = (
            ndep.reindex(cols, axis=1)
            .reindex(gdf.loc[gdf["ruwheid"] == k].index, axis=0)
            .to_numpy()[np.arange(len(ndep)), idx]
        )

        # Drop NAs
        if drop_na:
            gdf.drop(
                labels=gdf.loc[(gdf["ruwheid"] == k) & (gdf[target_name].isna())].index,
                inplace=True,
            )

        update = (
            f"Adding {target_name} information from {ndep_src[v]} to {pre:,} headers with ruwheid == {k}, "
            f"with {gdf.loc[gdf['ruwheid'] == k].shape[0]:,} remaining."
        )
        print(update)
        msg += f"\n{update}"

    return gdf, msg


def add_ndep_yearly(
    gdf: gp.GeoDataFrame,
    msg: str,
    target_name: str,
    ndep_src: dict,
    t_delta: int,
    aggfunc: str,
    drop_na: bool,
) -> (gp.GeoDataFrame, str):
    """
    Add N-deposition data to the plots, based on delivery by Max Posch of annual deposition data per plot.
    Calculates a summary statistic of annual NDep per plot, for the X years up to the year of plot-survey.

    The NDep dataset is build as follows:

    !Made by  depall.exe  from  eva_headers_20221220-1230.csv
    ! annual forest NH3-deposition (given year=last year of averaging)
    !-	deg	deg	mgN/m2	mgN/m2	mgN/m2	mgN/m2	mgN/m2	mgN/m2	mgN/m2
    !plot_obs_id	Lon	Lat	1980	1981	1982	1983	1984	1985	1986
    2795	-9.3974	39.1076	112.11	114.13	116.16	118.21	120.28	122.37	124.5
    2810	-9.4716	38.7306	139.71	140.42	141.13	141.84	142.54	143.23	143.92
    2811	-8.8978	40.2077	103.2	104.12	105.03	105.94	106.85	107.76	108.73

    Supose plot ID 2795 was surveyed in 1985.

    If t_delta=5 and aggfunc='sum', the outcome will be:
    ndep_2795 = 114.13+116.16+118.21+120.28+122.37=591.15 mgN/m2

    If t_delta=5 and aggfunc='max', the outcome will be:
    ndep_2795 = max(114.13, 116.16, 118.21, 120.28, 122.37) = 122.37

    If t_delta=6 and aggfunc='mean, the outcome will be:
    ndep_2795 = 117.21

    If t_delta=9999, NDep data will be used from the earliest year in the NDep delivery (1880) up to and including
    year of the survey.

    Parameters
    ----------
    gdf: GeoDataFrame where rows are plots and columsn are plot-metadata. Index is plot_ids
    msg: string carrying information on all processing steps
    target_name: name of the column to be added to *gdf*
    ndep_src: path to the CSV file with the relevant NDep data
    t_delta: integer, years up to and including the plot surveying year to consider. 9999 means: from start of data
    aggfunc: string, aggregation-function, one of 'mean', 'sum', 'minimum', 'maximum'
    drop_na: boolean, drop plots that where not assigned a new Ndep

    Returns
    -------
    gdf, str
    """

    def add_ndep(s: pd.Series, ndep_df: gp.GeoDataFrame, what: str):
        """
        Helper function to extract NDep data from *ndep_df*
        Parameters
        ----------
        s: Pandas Series with *name*: a plot_id, "year"= year of survey and "start_year"=start year
        ndep_df: NDep dataframe as read from file. With plot_ids as index and Integer year columns
        what: one of 'min', 'max', 'average', 'sum'

        Returns
        -------
        NDep for selected plot and over selected years summarized according to *what*
        """

        # Select 1 row and >= 1 columns from the NDep dataframe
        sel = ndep_df.loc[
            s.name, lambda df: (df.columns >= s.start_year) & (df.columns <= s.year)
        ]

        # Return the requested summary statistic
        return {
            "max": sel.max(),
            "min": sel.min(),
            "average": sel.mean(),
            "sum": sel.sum(),
        }[what]

    # N-deposition is differentiated between hoog (f) and laag (v) vegetation.
    d = {"hoog": "f", "laag": "v"}

    gdf['start_year'] = gdf.year.subtract(t_delta) if t_delta < 9999 else 1880

    for k, v in d.items():

        pre = gdf.loc[gdf["ruwheid"] == k].shape[0]

        # Read relevant CSV file from file
        ndep_df = read_ndep_file(src=ndep_src[v], headers_as_str=False)

        # Create a new selection from *gdf*. Add starting year for NDep calculation
        selected_headers = gdf.loc[gdf["ruwheid"] == k].index.intersection(ndep_df.index)

        # Apply "add_ndep" function over df rows: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.apply.html
        gdf.loc[selected_headers, f'{target_name}'] = gdf.loc[selected_headers, :].apply(add_ndep, axis=1, ndep_df=ndep_df,
                                                                                         what=aggfunc)

        timeinfo = 'from 1880 to survey-year' if t_delta == 9999 else f'for {t_delta} years before survey-year'
        update = (
            f"Adding {target_name} information from {ndep_src[v]} to {pre:,} headers with ruwheid == {k}. Calculate {aggfunc} of {target_name} {timeinfo}."
            f"with {gdf.loc[gdf['ruwheid'] == k].shape[0]:,} remaining."
        )
        print(update)
        msg += f"\n{update}"

    # Drop NAs in the new column if *drop_na* is True.
    if drop_na:
        gdf.dropna(subset=target_name, inplace=True)

    # Return *gdf* and *msg*
    return gdf.drop(columns='start_year'), msg


def add_n_totals(gdf: gp.GeoDataFrame, msg: str) -> (gp.GeoDataFrame, str):
    """
    Calculated total NDep based on NH3 and NOx
    :param gdf:
    :return:
    """

    # Calculate totals
    gdf["N_mg_m2"] = gdf.loc[:, ["nh3_mg_m2", "nox_mg_m2"]].sum(axis=1)
    gdf["N_kg_ha"] = gdf.loc[:, "N_mg_m2"].divide(100)
    gdf["N_kmol_ha"] = gdf.loc[:, "N_kg_ha"].divide(14)

    update = f"Adding total N in mg_m2, kg_ha and kmol_ha for {gdf.shape[0]:,} headers."
    print(update)
    return (gdf, f"{msg}\n{update}")


def add_structuurtype(
    gdf: gp.GeoDataFrame, msg: str, timestamp: str, out_dir: str, drop_na=True
) -> (gp.GeoDataFrame, str):
    """
    Add structuurtype kolom to dataframe, based on EUNIS code. Structuurtypen zijn:


    :param gdf:
    :param msg:
    :return:
    """

    mapper_part1 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_nieuw"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_nieuw"]["key"],
        value_column=SOURCE_DATA["structuurtypen_eunis_nieuw"]["value"],
        sheet=SOURCE_DATA["structuurtypen_eunis_nieuw"]["sheet"],
    )

    mapper_part2 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_oud"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_oud"]["key"],
        value_column=SOURCE_DATA["structuurtypen_eunis_oud"]["value"],
        sheet=SOURCE_DATA["structuurtypen_eunis_oud"]["sheet"],
    )

    mapper = mapper_part1 | mapper_part2

    pre = gdf.shape[0]
    gdf["structuur"] = gdf.eunis_code.map(mapper)

    unmapped_eunis = set(gdf.eunis_code).difference(set(mapper.keys()))

    if unmapped_eunis:
        value_counts = (
            gdf.loc[gdf.eunis_code.isin(unmapped_eunis)]
            .eunis_code.value_counts()
            .sort_values(ascending=True)
        )

        pd.DataFrame(
            {"non-mapped EUNIS": value_counts.index, "# headers": value_counts.values}
        ).to_csv(
            os.path.join(out_dir, f"{timestamp}_missing_eunis.csv"),
            index=False,
            sep="\t",
        )

    if drop_na:
        gdf = gdf.dropna(subset=["structuur"])

    update = (
        f"Adding structuurtype from {SOURCE_DATA['structuurtypen_eunis_nieuw']['src']}"
        f"-{SOURCE_DATA['structuurtypen_eunis_nieuw']['sheet']}-"
        f"{SOURCE_DATA['structuurtypen_eunis_nieuw']['value']} and {SOURCE_DATA['structuurtypen_eunis_oud']['src']}"
        f"-{SOURCE_DATA['structuurtypen_eunis_oud']['sheet']}-"
        f"{SOURCE_DATA['structuurtypen_eunis_oud']['value']} to {pre:,} headers, with {gdf.shape[0]:,} "
        f"headers remaining. Note {len(unmapped_eunis)} EUNIS-type(s) not mapped to a structuurtype."
    )
    print(update)
    return (gdf, f"{msg}{update}")


def add_roughness(
    gdf: gp.GeoDataFrame, msg: str, drop_na=True
) -> (gp.GeoDataFrame, str):
    """
    Add roughness classification of the plot, based on EUNIS type. Roughness is: {high, low}
    :param gdf:
    :param msg:
    :return:
    """

    mapper_part1 = dict_from_file(
        src=SOURCE_DATA["ruwheid_eunis_nieuw"]["src"],
        key_column=SOURCE_DATA["ruwheid_eunis_nieuw"]["key"],
        value_column=SOURCE_DATA["ruwheid_eunis_nieuw"]["value"],
        sheet=SOURCE_DATA["ruwheid_eunis_nieuw"]["sheet"],
    )

    mapper_part2 = dict_from_file(
        src=SOURCE_DATA["ruwheid_eunis_oud"]["src"],
        key_column=SOURCE_DATA["ruwheid_eunis_oud"]["key"],
        value_column=SOURCE_DATA["ruwheid_eunis_oud"]["value"],
        sheet=SOURCE_DATA["ruwheid_eunis_oud"]["sheet"],
    )

    mapper = mapper_part1 | mapper_part2

    pre = gdf.shape[0]
    gdf["ruwheid"] = gdf.eunis_code.map(mapper)

    if drop_na:
        gdf = gdf.dropna(subset=["ruwheid"])

    update = (
        f"Adding ruwheid from {SOURCE_DATA['ruwheid_eunis_nieuw']['src']}-"
        f"{SOURCE_DATA['ruwheid_eunis_nieuw']['sheet']}-{SOURCE_DATA['ruwheid_eunis_nieuw']['value']} and "
        f"{SOURCE_DATA['ruwheid_eunis_oud']['src']}-"
        f"{SOURCE_DATA['ruwheid_eunis_oud']['sheet']}-{SOURCE_DATA['ruwheid_eunis_oud']['value']}"
        f"to {pre:,} headers, with {gdf.shape[0]:,} headers remaining."
    )
    print(update)
    return (gdf, f"{msg}\n{update}")


def merge_old_new_eunis(df: pd.DataFrame, msg: str) -> (pd.DataFrame, str):
    """
    Create single EUNIS column based on eunis_new where possible, else eunis_old
    :param df:
    :return:
    """

    df["eunis_code"] = pd.Series(
        np.where(
            df.eunis_new.isna(),
            df.eunis_old,
            df.eunis_new,
        ),
        index=df.index,
    ).str.replace(",", "-")
    update = f"Merging eunis-old and eunis-new codes for {df.shape[0]:,} headers."
    print(update)
    return (df, f"{msg}\n{update}")


def composite_eunis_to_single(
    gdf: pd.DataFrame, msg: str, drop_na=False
) -> (pd.DataFrame, str):
    """
    Some headers have a composite EUNIS type, eg. "G1Aa-G1Ab". Reduce these to first EUNIS type: "G1Aa"
    EUNIS codes are split on: "-" "!-" "," "!,"
    :param df:
    :param msg:
    :return:
    """

    split_code = gdf.eunis_code.str.split(r"-|!-|,|!,")
    n = pd.Series([len(x) for x in split_code]).value_counts().iloc[1:].sum()
    gdf["eunis_code"] = split_code.str[0]

    if drop_na:
        gdf = gdf.dropna(subset=["ruwheid"])
    update = f"Reducing composite eunis-code to single eunis for {n} headers, with {gdf.shape[0]} remaining."
    print(update)
    return (gdf, f"{msg}\n{update}")


def overwrite_eunis(
    gdf: gp.GeoDataFrame,
    msg: str,
    mapper=EUNIS_OVERWRITE,
) -> (gp.GeoDataFrame, str):
    """
    Overwrite EUNIS type of selected plots
    :param src: path to CSV with plots that require overwrite
    :param src_col: plot identifying column in src
    :param target_eunis: target EUNIS type
    :return:
    """

    msg += "\n"

    for src, target_eunis in mapper.items():
        target_ids = pd.read_csv(
            src, sep="\t", index_col="PlotObservationID"
        ).index.intersection(gdf.index)

        gdf.loc[target_ids, "eunis_code"] = target_eunis
        update = f"Setting EUNIS type to {target_eunis} based on {src} for {len(target_ids):,} headers."
        print(update)
        msg += f"{update}\n"

    return gdf, msg


def dict_from_file(src: str, key_column: str, value_column: str, **kwargs) -> dict:
    """
    Read two columns from a table from file and return as dictionary
    :param src:
    :param key_column:
    :param value_column:
    :param kwargs:
    :return:
    """

    dbf = f"{os.path.splitext(src)[0]}.tif.vat.dbf"
    csv = f"{os.path.splitext(src)[0]}.csv"
    xlsx = f"{os.path.splitext(src)[0]}.xlsx"

    if os.path.isfile(dbf):
        out = getattr(gp.read_file(dbf).set_index(key_column), value_column).to_dict()
    elif os.path.isfile(csv):
        out = getattr(
            pd.read_csv(
                csv,
                sep=kwargs.get("sep"),
                comment="#",
            ).set_index(key_column, drop=False),
            value_column,
        ).to_dict()
    elif os.path.isfile(xlsx):
        try:
            out = getattr(
                pd.read_excel(src, sheet_name=kwargs.get("sheet")).set_index(
                    key_column, drop=False
                ),
                value_column,
            ).to_dict()
        except KeyError as e:
            print(e)
            return None
    else:
        print("No valid attribute table found.")
        out = None
    return out


def species_name_translator(
    identifier: str, is_what: str, return_as: str, strict=True
) -> str:
    """
    Map a species identifier to its equivalent.
    :param identifier: string to identify a species
    :param is_what: is *identifier* a Dutch name, Latin name or Species code
    :param return_as: return as either a Dutch name, Latin name or Species code
    :return:
    """

    assert is_what in [
        "wetenschappelijke_naam",
        "nederlandse_naam",
        "soortnr",
    ], f"invalid is_what: {is_what}"
    assert return_as in [
        "wetenschappelijke_naam",
        "nederlandse_naam",
        "soortnr",
    ], f"invalid return_as: {is_what}"

    d = dict_from_file(
        src=SOURCE_DATA["doren_master_excel"],
        key_column=is_what,
        value_column=return_as,
        sheet="soorten_RL",
    )
    try:
        return d[identifier]
    except KeyError:
        if strict:
            raise KeyError(f"This is not a plant: {identifier}")
        else:
            return "unknown species"


def habitat_code_to_omschrijving(code):
    """
    Map Habitattypecode to omschrijving.
    Eg 1310-A -> Zulte pionierbegroeiiingen (zeekraal)
    Parameters
    ----------
    code

    Returns
    -------

    """

    d = dict_from_file(
        src=SOURCE_DATA["doren_master_excel"],
        key_column="Hcode",
        value_column="habitattype",
        sheet="habitattypen",
    )

    try:
        return d[code]
    except KeyError:
        return "geen habitattype"


def eunis_code_to_type() -> str:
    """
    Translate a EUNIS code to type
    """
    d1 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_nieuw"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_nieuw"]["key"],
        value_column="eunis_omschrijving",
        sheet=SOURCE_DATA["structuurtypen_eunis_nieuw"]["sheet"],
    )

    d2 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_oud"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_oud"]["key"],
        value_column=SOURCE_DATA["structuurtypen_eunis_oud"]["key"],
        sheet=SOURCE_DATA["structuurtypen_eunis_oud"]["sheet"],
    )
    out = {}
    for k, v in dict(d1 | d2).items():
        if k != v:
            out[k] = f"{k}: {v}"
        else:
            out[k] = v

    return out


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def write_specs_to_file(out_directory, outname, **kwargs):
    """
    Write all specifications to json
    :param out_directory:
    :param outname:
    :param kwargs:
    :return:
    """

    with open(os.path.join(out_directory, f"{outname}.json"), "w") as dest:
        json.dump(kwargs, dest)


def simplify_species(species_name: str) -> str:
    """
    Function to simplify plant species name to just family-species by removing subspecies etc
    Eg. Montia fontana subsp. fontana --> Montia fontana
    :param species_name:
    :return: species_name minus subsp or other flags
    """
    pattern = re.compile(r'subsp.? | var.? | aggr.? | ""| ssp.? | s.? | mod.? | \(| aggr\.?$| sensu\.?| s.l.')

    if re.search(pattern, species_name):
        out = re.split(pattern, species_name)[0]
    else:
        out = species_name
    return out.strip()


def species_list_(df: pd.DataFrame) -> pd.DataFrame:
    """
    Build dataframe with species lists. Columns are:
    matched_concept: all unique Latin species names from SOURCE_DATA["eva_species"]
    pre_filter: count of @ in SOURCE_DATA["eva_species"]
    post_filter: count of @ in <timestamp>_eva_headers.csv
    simplified: simplified version of @ with subspecies indicators removed.
    pre_filter_simplified: count of @ in SOURCE_DATA["eva_species"]
    post_filter_simplified: count of @ in <timestamp>_eva_headers.csv

    NOTE: when > 1 matched_concept correspond to 1 simplified-concept, the last two columns are NA. EG

    matched_concept                     |pre_filter|post_filter|simplified          |pre_filter_simplified|post_filter_simplified
    Abietinella abietina                |6283      |1352       |Abietinella abietina|7331                 |136
    Abietinella abietina var. abietina  |1033      |13         |Abietinella abietina|                     |
    Abietinella abietina var. hystricosa|15        |0          |Abietinella abietina|                     |
    NOTE2: 7.331 = 16.284+1.033+15


    Parameters
    ----------
    df: dataframe with unique matched concept as index.

    Returns
    -------

    """

    species = pd.read_csv(
        SOURCE_DATA["eva_species"],
        sep="\t",
        usecols=[0, 5],
        header=0,
        names=["plot_obs_id", "matched_concept"],
    )
    species['selected'] = np.where(species.plot_obs_id.isin(df.index), 1, 0)

    matched_concept_count_pre = pd.value_counts(species.matched_concept)
    matched_concept_count_post = pd.value_counts(species.loc[species.selected == 1, 'matched_concept'])
    df01 = pd.DataFrame({'pre_filter': matched_concept_count_pre,
                         'post_filter': matched_concept_count_post}).fillna(0)

    simplifier = matched_concept_count_pre.index.to_series().apply(simplify_species).to_dict()
    species['simplified_concept'] = species.matched_concept.map(simplifier)
    df01['simplified'] = df01.index.map(simplifier)

    simplified_concept_count_pre = pd.value_counts(species.simplified_concept)
    simplified_concept_count_post = pd.value_counts(species.loc[species.selected == 1, 'simplified_concept'])
    df02 = pd.DataFrame({'pre_filter': simplified_concept_count_pre,
                         'post_filter': simplified_concept_count_post}).fillna(0)

    df01 = df01.join(df02, how='left', on='simplified',
                     rsuffix='_simplified')
    df01.loc[
        df01.duplicated(subset=['simplified',
                                'pre_filter_simplified',
                                'post_filter_simplified']), ['pre_filter_simplified',
                                                             'post_filter_simplified']] = np.nan
    return df01


def eva_species_list_selected(df: pd.DataFrame) -> pd.DataFrame:
    """
    Read the EVA species list and return with selected plots only
    Parameters
    ----------
    df: header dataframe

    Returns
    -------

    """

    species = pd.read_csv(
        SOURCE_DATA["eva_species"],
        sep="\t",
        usecols=["PlotObservationID", "Matched concept", "Cover %",	"Cover code"],
        header=0,
    )

    return species.loc[species.PlotObservationID.isin(df.index), :]


# lst = ["Abietinella abietina", "Achillea millefolium", "Achillea ptarmica", "Aconitum lycoctonum subsp. vulparia", "Actaea spicata", "Adoxa moschatellina", "Aegopodium podagraria", "Agrimonia eupatoria", "Agrimonia procera", "Agrostis canina", "Agrostis capillaris", "Agrostis gigantea", "Agrostis stolonifera", "Agrostis vinealis", "Aira caryophyllea", "Aira praecox", "Ajuga reptans", "Alchemilla glabra", "Alliaria petiolata", "Allium oleraceum", "Allium scorodoprasum", "Allium ursinum", "Allium vineale", "Alopecurus bulbosus", "Alopecurus pratensis", "Alyssum alyssoides", "Amblystegium serpens", "Amelanchier lamarckii", "Ammophila arenaria", "Anacamptis morio", "Anacamptis pyramidalis", "Anagallis minima", "Anagallis tenella", "Andromeda polifolia", "Anemone nemorosa", "Anemone ranunculoides", "Aneura pinguis", "Angelica archangelica", "Angelica sylvestris", "Anisantha tectorum", "Antennaria dioica", "Anthoxanthum odoratum", "Anthriscus caucalis", "Anthriscus sylvestris", "Anthyllis vulneraria", "Aquilegia vulgaris", "Arabis glabra", "Arabis hirsuta s. hirsuta", "Arctium minus s.l.", "Arctostaphylos uva-ursi", "Arenaria serpyllifolia", "Argentina anserina", "Aristavena setacea", "Armeria maritima", "Arnica montana", "Aronia floribunda", "Arrhenatherum elatius", "Artemisia campestris s. campestris", "Artemisia campestris s. maritima", "Artemisia maritima", "Artemisia vulgaris", "Arum italicum", "Arum maculatum", "Asparagus officinalis s. officinalis", "Asparagus officinalis s. prostratus", "Astomum crispum", "Athyrium filix-femina", "Atrichum undulatum", "Atriplex littoralis", "Atriplex prostrata", "Aulacomnium androgynum", "Aulacomnium palustre", "Avenella flexuosa", "Avenula pubescens", "Azolla filiculoides", "Baldellia ranunculoides s. ranunculoides", "Baldellia ranunculoides s. repens", "Barbilophozia barbata", "Barbula convoluta", "Barbula unguiculata", "Barbula vinealis", "Bazzania trilobata", "Bellis perennis", "Berberis vulgaris", "Betula pubescens", "Blackstonia perfoliata s. serotina", "Blackstonia perfoliata s.l.", "Blysmus compressus", "Blysmus rufus", "Botrychium lunaria", "Brachypodium pinnatum", "Brachypodium sylvaticum", "Brachythecium albicans", "Brachythecium glareosum", "Brachythecium rivulare", "Brachythecium rutabulum", "Brachythecium velutinum", "Briza media", "Bromopsis erecta", "Bromopsis inermis s. inermis", "Bromopsis ramosa ssp. benekenii", "Bromopsis ramosa ssp. ramosa", "Bromus hordeaceus s. hordeaceus", "Bromus hordeaceus s. thominei", "Bromus racemosus s. racemosus", "Bromus racemosus s.l.", "Bryoerythrophyllum recurvirostre", "Bryonia dioica", "Bryum caespiticium", "Bryum capillare", "Bryum pseudotriquetrum", "Bryum rubens", "Bupleurum tenuissimum", "Cakile maritima", "Calamagrostis canescens", "Calamagrostis epigejos", "Calamagrostis neglecta", "Calamagrostis × calammophila", "Calliergon cordifolium", "Calliergon giganteum", "Calliergonella cuspidata", "Callitriche obtusangula", "Callitriche stagnalis", "Calluna vulgaris", "Caltha palustris s. palustris", "Caltha palustris s.l.", "Calypogeia fissa", "Calystegia soldanella", "Campanula glomerata", "Campanula persicifolia", "Campanula rapunculus", "Campanula rotundifolia", "Campanula trachelium", "Campyliadelphus chrysophyllus", "Campylium stellatum", "Campylopus flexuosus", "Campylopus fragilis", "Campylopus introflexus", "Campylopus pyriformis", "Cardamine amara", "Cardamine hirsuta", "Cardamine pratensis", "Carduus crispus", "Carduus nutans", "Carex acuta", "Carex acutiformis", "Carex arenaria", "Carex canescens", "Carex caryophyllea", "Carex colchica", "Carex cuprina", "Carex diandra", "Carex digitata", "Carex dioica", "Carex distans", "Carex disticha", "Carex echinata", "Carex elata", "Carex elongata", "Carex ericetorum", "Carex extensa", "Carex flacca", "Carex flava", "Carex hartmanii", "Carex hirta", "Carex hostiana", "Carex laevigata", "Carex lasiocarpa", "Carex lepidocarpa", "Carex limosa", "Carex nigra", "Carex panicea", "Carex pendula", "Carex pilulifera", "Carex pseudocyperus", "Carex pulicaris", "Carex remota", "Carex rostrata", "Carex strigosa", "Carex sylvatica", "Carex trinervis", "Carex viridula", "Carlina vulgaris", "Carum carvi", "Carum verticillatum", "Catapodium rigidum", "Centaurea jacea s.l.", "Centaurea scabiosa", "Centaurium littorale", "Centaurium pulchellum", "Cephalanthera damasonium", "Cephalozia bicuspidata", "Cephalozia connivens", "Cephaloziella divaricata", "Cerastium arvense", "Cerastium diffusum", "Cerastium fontanum s. vulgare", "Cerastium pumilum", "Cerastium semidecandrum", "Ceratocapnos claviculata", "Ceratodon purpureus", "Ceratophyllum demersum", "Ceratophyllum submersum", "Cetraria aculeata", "Cetraria islandica", "Chaerophyllum bulbosum", "Chaerophyllum temulum", "Chamaecyparis lawsoniana", "Chara aspera", "Chara contraria", "Chara globularis", "Chara vulgaris", "Chrysosplenium alternifolium", "Chrysosplenium oppositifolium", "Cicendia filiformis", "Circaea alpina", "Circaea lutetiana", "Circaea x intermedia", "Cirsium acaulon", "Cirsium dissectum", "Cirsium palustre", "Cirsium vulgare", "Cladium mariscus", "Cladonia arbuscula", "Cladonia cariosa", "Cladonia cervicornis", "Cladonia chlorophaea", "Cladonia ciliata", "Cladonia coccifera", "Cladonia crispata", "Cladonia floerkeana", "Cladonia foliacea", "Cladonia furcata", "Cladonia glauca", "Cladonia gracilis", "Cladonia grayi", "Cladonia macilenta", "Cladonia phyllophora", "Cladonia pocillum", "Cladonia portentosa", "Cladonia ramulosa", "Cladonia rangiformis", "Cladonia squamosa", "Cladonia strepsilis", "Cladonia subulata", "Cladonia uncialis", "Cladonia verticillata", "Cladonia zopfii", "Cladopodiella fluitans", "Claytonia perfoliata", "Clematis vitalba", "Climacium dendroides", "Clinopodium acinos", "Clinopodium vulgare", "Cochlearia danica", "Cochlearia officinalis s. anglica", "Colchicum autumnale", "Comarum palustre", "Convallaria majalis", "Convolvulus arvensis", "Cornus sanguinea", "Corydalis cava", "Corydalis solida", "Corylus avellana", "Corynephorus canescens", "Crassula helmsii", "Crataegus germanica", "Crataegus laevigata", "Crataegus monogyna", "Cratoneuron filicinum", "Crepis biennis", "Crepis capillaris", "Crepis paludosa", "Cruciata laevipes", "Ctenidium molluscum", "Cuscuta epithymum", "Cynodon dactylon", "Cynoglossum officinale", "Cynosurus cristatus", "Cytisus scoparius", "Dactylis glomerata", "Dactylorhiza maculata", "Dactylorhiza majalis s. majalis", "Dactylorhiza majalis s. praetermissa", "Dactylorhiza majalis s.l.", "Dactylorhiza majalis ssp. sphagnicola", "Dactylorhiza viridis", "Danthonia decumbens", "Daphne mezereum", "Daucus carota", "Deschampsia cespitosa", "Dianthus deltoides", "Dichoropetalum carvifolia", "Dicranella heteromalla", "Dicranodontium denudatum", "Dicranoweisia cirrata", "Dicranum bonjeanii", "Dicranum montanum", "Dicranum polysetum", "Dicranum scoparium", "Digitalis purpurea", "Diplotaxis tenuifolia", "Dipsacus pilosus", "Ditrichum flexicaule", "Ditrichum pallidum", "Draba verna", "Drepanocladus aduncus", "Drepanocladus polygamus", "Drosera intermedia", "Drosera longifolia", "Drosera rotundifolia", "Dryopteris carthusiana", "Dryopteris cristata", "Dryopteris dilatata", "Dryopteris filix-mas", "Echium vulgare", "Elatine hexandra", "Eleocharis acicularis", "Eleocharis multicaulis", "Eleocharis palustris", "Eleocharis quinqueflora", "Eleocharis uniglumis", "Elodea nuttallii", "Elytrigia atherica", "Elytrigia juncea s. boreoatlantica", "Elytrigia repens", "Empetrum nigrum", "Encalypta streptocarpa", "Encalypta vulgaris", "Entodon concinnus", "Epilobium angustifolium", "Epilobium ciliatum", "Epilobium montanum", "Epilobium palustre", "Epilobium parviflorum", "Epilobium tetragonum", "Epipactis helleborine", "Epipactis palustris", "Equisetum fluviatile", "Equisetum hyemale", "Equisetum palustre", "Equisetum sylvaticum", "Equisetum telmateia", "Equisetum variegatum", "Erica cinerea", "Erica tetralix", "Erigeron acris", "Eriophorum angustifolium", "Eriophorum gracile", "Eriophorum latifolium", "Eriophorum vaginatum", "Erodium cicutarium s. dunense", "Erodium lebelii", "Eryngium campestre", "Eryngium maritimum", "Erysimum cheiranthoides", "Euonymus europaeus", "Eupatorium cannabinum", "Euphorbia amygdaloides", "Euphorbia cyparissias", "Euphorbia esula", "Euphorbia esula s. esula", "Euphorbia paralias", "Euphorbia seguieriana", "Euphrasia officinalis (= rostkoviana)", "Euphrasia stricta", "Eurhynchium hians", "Eurhynchium striatum", "Evernia prunastra", "Fallopia dumetorum", "Festuca arenaria", "Festuca arundinacea", "Festuca brevipila", "Festuca filiformis", "Festuca ovina", "Festuca rubra", "Ficaria verna s.l.", "Filago minima", "Filipendula ulmaria", "Fissidens adianthoides", "Fissidens bryoides", "Fissidens dubius", "Fissidens taxifolius", "Fontinalis antipyretica", "Fragaria vesca", "Frangula alnus", "Fritillaria meleagris", "Gagea spathacea", "Galanthus nivalis", "Galeopsis tetrahit", "Galium aparine", "Galium boreale", "Galium mollugo", "Galium odoratum", "Galium palustre s.l.", "Galium pumilum", "Galium saxatile", "Galium sylvaticum", "Galium uliginosum", "Galium verum s.l.", "Genista anglica", "Genista pilosa", "Genista tinctoria", "Gentiana cruciata", "Gentiana pneumonanthe", "Gentianella amarella", "Gentianella campestris", "Gentianella ciliata", "Gentianella germanica", "Geranium dissectum", "Geranium molle", "Geranium pratense", "Geranium pyrenaicum", "Geranium robertianum", "Geum rivale", "Geum urbanum", "Glaux maritima", "Glechoma hederacea", "Glyceria fluitans", "Glyceria maxima", "Gnaphalium uliginosum", "Gymnadenia conopsea", "Gymnocolea inflata", "Halimione pedunculata", "Hamatocaulis vernicosus", "Hedera helix", "Helianthemum nummularium", "Helictotrichon pratensis", "Helosciadium inundatum", "Heracleum sphondylium", "Herniaria glabra", "Hieracium murorum", "Hieracium sect. Vulgata", "Hieracium umbellatum s.l.", "Hierochloë odorata", "Hippocrepis comosa", "Hippophaë rhamnoides", "Hippuris vulgaris", "Holcus lanatus", "Holcus mollis", "Homalothecium lutescens", "Honckenya peploides", "Hordeum marinum", "Humulus lupulus", "Hydrocharis morsus-ranae", "Hydrocotyle vulgaris", "Hylocomium splendens", "Hylotelephium telephium", "Hypericum elodes", "Hypericum hirsutum", "Hypericum perforatum", "Hypericum pulchrum", "Hypericum tetrapterum", "Hypnum cupressiforme", "Hypnum imponens", "Hypochaeris radicata", "Hypogymnia physodes", "Hypopitys monotropa", "Ilex aquifolium", "Impatiens glandulifera", "Impatiens noli-tangere", "Inula conyzae", "Iris pseudacorus", "Isolepis fluitans", "Isolepis setacea", "Jacobaea aquatica", "Jacobaea erucifolia", "Jacobaea paludosa", "Jacobaea vulgaris s. dunensis", "Jacobaea vulgaris s. vulgaris", "Jacobaea vulgaris s.l.", "Jasione montana", "Juncus acutiflorus", "Juncus articulatus", "Juncus balticus", "Juncus bufonius", "Juncus bulbosus s.l.", "Juncus capitatus", "Juncus compressus", "Juncus conglomeratus", "Juncus effusus", "Juncus filiformis", "Juncus gerardi", "Juncus inflexus", "Juncus maritimus", "Juncus pygmaeus", "Juncus squarrosus", "Juncus subnodulosus", "Juncus tenageia", "Juniperus communis", "Knautia arvensis", "Koeleria macrantha", "Koeleria pyramidata", "Kurzia pauciflora", "Lamium album", "Lamium galeobdolon", "Laphangium luteoalbum", "Lapsana communis", "Lathyrus nissolia", "Lathyrus palustris", "Lathyrus pratensis", "Lemna minor", "Leontodon hispidus", "Lepidozia reptans", "Lepraria incana", "Leptobryum pyriforme", "Leskea polycarpa", "Leucanthemum vulgare", "Leucobryum glaucum", "Leymus arenarius", "Ligustrum vulgare", "Limonium vulgare", "Limosella aquatica", "Linum catharticum", "Liparis loeselii", "Lithospermum officinale", "Littorella uniflora", "Lobelia dortmanna", "Lolium perenne", "Lonicera periclymenum", "Lonicera xylosteum", "Lophocolea bidentata", "Lophozia ventricosa", "Lotus corniculatus", "Lotus maritimus", "Lotus pedunculatus", "Lotus tenuis", "Luronium natans", "Luzula campestris", "Luzula luzuloides", "Luzula multiflora s. multiflora", "Luzula multiflora s.l.", "Luzula pilosa", "Luzula sylvatica", "Lycopodiella inundata", "Lycopodium clavatum", "Lycopus europaeus", "Lysimachia nemorum", "Lysimachia nummularia", "Lysimachia thyrsiflora", "Lysimachia vulgaris", "Lythrum portula", "Lythrum salicaria", "Maianthemum bifolium", "Marchantia polymorpha", "Medicago falcata", "Medicago lupulina", "Medicago minima", "Melampyrum pratense", "Melica nutans", "Melica uniflora", "Mentha aquatica", "Menyanthes trifoliata", "Mercurialis perennis", "Milium effusum", "Minuartia hybrida", "Mnium hornum", "Moehringia trinervia", "Moenchia erecta", "Molinia caerulea", "Mylia anomala", "Myosotis laxa s. cespitosa", "Myosotis ramosissima", "Myosotis scorpioides s.l.", "Myosotis sylvatica", "Myosoton aquaticum", "Myrica gale", "Myriophyllum alterniflorum", "Myriophyllum spicatum", "Najas marina", "Nardus stricta", "Narthecium ossifragum", "Nasturtium microphyllum/officinale", "Neottia nidus-avis", "Neottia ovata", "Nitella flexilis", "Nitellopsis obtusa", "Noccaea caerulescens", "Noccaea perfoliata", "Odontites vernus s. serotinus", "Odontites vernus s.l.", "Odontoschisma sphagni", "Oenanthe lachenalii", "Ononis spinosa", "Ophioglossum vulgatum", "Ophrys insectifera", "Orchis anthropophora", "Orchis mascula", "Orchis militaris", "Orchis purpurea", "Orchis simia", "Origanum vulgare", "Ornithogalum umbellatum", "Ornithopus perpusillus", "Orobanche caryophyllacea", "Orobanche hederae", "Orobanche lutea", "Orthodontium lineare", "Oxalis acetosella", "Oxybasis glauca", "Oxybasis rubra", "Oxyrrhynchium pumilum", "Pallavicinia lyellii", "Palustriella commutata", "Parapholis strigosa", "Paris quadrifolia", "Parnassia palustris", "Pastinaca sativa s. sativa", "Pedicularis palustris", "Pedicularis sylvatica", "Pellia endiviifolia", "Pellia epiphylla", "Pellia neesiana", "Peltigera rufescens", "Persicaria amphibia", "Persicaria lapathifolia s.l.", "Petrorhagia prolifera", "Peucedanum palustre", "Phalaroides arundinacea", "Phleum arenarium", "Phleum pratense s.l.", "Phragmites australis", "Phyteuma spicatum ssp. nigrum", "Phyteuma spicatum ssp. spicatum", "Picris hieracioides", "Pilosella officinarum", "Pilosella peleteriana", "Pilularia globulifera", "Pimpinella major", "Pimpinella saxifraga", "Pinguicula vulgaris", "Pinus sylvestris", "Placidium rufescens", "Placynthiella uliginosa", "Plagiochila asplenioides", "Plagiomnium affine", "Plagiomnium cuspidatum", "Plagiomnium elatum", "Plagiomnium undulatum", "Plagiothecium denticulatum", "Plagiothecium laetum", "Plagiothecium nemorale", "Plantago coronopus", "Plantago lanceolata", "Plantago major s. intermedia", "Plantago maritima", "Plantago media", "Platanthera bifolia", "Platanthera chlorantha", "Platismatia glauca", "Pleurochaete squarrosa", "Poa angustifolia", "Poa bulbosa", "Poa compressa", "Poa nemoralis", "Poa palustris", "Poa pratensis", "Poa trivialis", "Pohlia nutans", "Polygala comosa", "Polygala serpyllifolia", "Polygala vulgaris", "Polygonatum multiflorum", "Polygonatum odoratum", "Polygonatum verticillatum", "Polypodium vulgare", "Polystichum aculeatum", "Polytrichastrum formosum", "Polytrichastrum longisetum", "Polytrichum commune", "Polytrichum juniperinum", "Polytrichum piliferum", "Potamogeton coloratus", "Potamogeton crispus", "Potamogeton friesii", "Potamogeton gramineus", "Potamogeton lucens", "Potamogeton natans", "Potamogeton nodosus", "Potamogeton perfoliatus", "Potamogeton polygonifolius", "Potamogeton trichoides", "Potentilla argentea", "Potentilla erecta", "Potentilla reptans", "Potentilla sterilis", "Potentilla supina", "Potentilla tabernaemontani", "Preissia quadrata", "Primula acaulis", "Primula elatior", "Primula veris", "Prunella vulgaris", "Prunus padus", "Prunus serotina", "Prunus spinosa", "Pseudocrossidium hornschuchianum", "Pseudoscleropodium purum", "Pseudotaxiphyllum elegans", "Psora decipiens", "Pteridium aquilinum", "Ptilidium ciliare", "Puccinellia distans s. borealis", "Puccinellia distans s. distans", "Puccinellia distans s.l.", "Puccinellia maritima", "Puccinellia pseudodistans", "Pulicaria dysenterica", "Pulicaria vulgaris", "Pyrola minor", "Pyrola rotundifolia", "Racomitrium canescens s.l.", "Radiola linoides", "Ranunculus acris", "Ranunculus auricomus", "Ranunculus bulbosus", "Ranunculus circinatus", "Ranunculus flammula", "Ranunculus lingua", "Ranunculus peltatus s.l.", "Ranunculus peltatus subsp. baudotii", "Ranunculus polyanthemos s. polyanthemoides", "Ranunculus polyanthemos s.l.", "Ranunculus polyanthemos ssp. nemorosus", "Ranunculus repens", "Ranunculus sardous", "Reseda lutea", "Reseda luteola", "Rhamnus cathartica", "Rhinanthus alectorolophus", "Rhinanthus angustifolius", "Rhinanthus minor", "Rhizomnium pseudopunctatum", "Rhodobryum roseum", "Rhynchospora alba", "Rhynchospora fusca", "Rhytidiadelphus squarrosus", "Rhytidiadelphus triquetrus", "Ribes nigrum", "Riccardia multifida", "Riccardia sinuata", "Rinodina griseosoralifera", "Rorippa palustris", "Rorippa sylvestris", "Rosa arvensis", "Rosa canina s.l.", "Rosa rubiginosa s.l.", "Rosa spinosissima", "Rubus caesius", "Rubus fruticosus", "Rubus idaeus", "Rumex acetosa", "Rumex acetosella", "Rumex conglomeratus", "Rumex maritimus", "Rumex obtusifolius", "Rumex obtusifolius s. transiens", "Rumex palustris", "Rumex sanguineus", "Rumex thyrsiflorus", "Ruppia maritima", "Sagina maritima", "Sagina nodosa", "Sagina procumbens", "Sagittaria sagittifolia", "Salicornia europaea sensu FvN 23", "Salicornia procumbens sensu FvN 23", "Salix alba", "Salix aurita x cinerea", "Salix caprea", "Salix cinerea", "Salix fragilis", "Salix gmelinii", "Salix purpurea", "Salix repens", "Salix triandra", "Salix viminalis", "Salsola kali", "Salvia pratensis", "Sambucus nigra", "Sambucus racemosa", "Samolus valerandi", "Sanguisorba minor", "Sanguisorba officinalis", "Sanicula europaea", "Saponaria officinalis", "Saxifraga tridactylites", "Scabiosa columbaria", "Scapania irrigua", "Scapania nemorea", "Schedonorus giganteus", "Schedonorus pratensis", "Scheuchzeria palustris", "Schoenus nigricans", "Scirpus lacustris", "Scirpus sylvaticus", "Scleranthus annuus", "Scleranthus annuus s. polycarpos", "Scleranthus perennis", "Scorpidium scorpioides", "Scorzonera humilis", "Scorzoneroides autumnalis", "Scrophularia auriculata", "Scrophularia nodosa", "Scutellaria galericulata", "Scutellaria minor", "Sedum acre", "Sedum rupestre", "Sedum sexangulare", "Selinum carvifolia", "Senecio nemorensis s. fuchsii", "Senecio sylvaticus", "Senecio viscosus", "Silaum silaus", "Silene baccifera", "Silene conica", "Silene dioica", "Silene flos-cuculi", "Silene latifolia s. alba", "Silene nutans", "Silene otites", "Silene vulgaris", "Solanum dulcamara", "Solidago virgaurea", "Sonchus arvensis", "Sonchus arvensis s.l.", "Sorbus aucuparia", "Sparganium angustifolium", "Sparganium emersum", "Sparganium natans", "Spartina anglica", "Spartina maritima", "Spergula morisonii", "Spergularia marina", "Spergularia media s. angustata", "Spergularia rubra", "Sphagnum angustifolium", "Sphagnum auriculatum", "Sphagnum compactum", "Sphagnum contortum", "Sphagnum cuspidatum", "Sphagnum fallax", "Sphagnum fimbriatum", "Sphagnum flexuosum", "Sphagnum magellanicum", "Sphagnum majus", "Sphagnum molle", "Sphagnum palustre", "Sphagnum papillosum", "Sphagnum pulchrum", "Sphagnum rubellum", "Sphagnum russowii", "Sphagnum squarrosum", "Sphagnum subnitens", "Sphagnum tenellum", "Spirodela polyrhiza", "Stachys officinalis", "Stachys palustris", "Stachys sylvatica", "Stellaria graminea", "Stellaria holostea", "Stellaria nemorum", "Stellaria pallida", "Stellaria palustris", "Straminergon stramineum", "Stratiotes aloides", "Stuckenia pectinata", "Suaeda maritima", "Succisa pratensis", "Symphytum officinale", "Syntrichia ruralis", "Tanacetum vulgare", "Taraxacum nordstedtii", "Taraxacum sect. Erythrosperma", "Taraxacum tortilobum", "Teesdalia nudicaulis", "Tetraphis pellucida", "Teucrium botrys", "Teucrium montanum", "Teucrium scordium", "Teucrium scorodonia", "Thalictrum flavum", "Thalictrum minus", "Thamnobryum alopecurum", "Thelypteris palustris", "Thuidium assimile", "Thuidium delicatulum", "Thymus praecox", "Thymus pulegioides", "Thymus serpyllum", "Tomentypnum nitens", "Torilis japonica", "Tortella flavovirens", "Tortula subulata", "Tragopogon pratensis", "Tragopogon pratensis s. pratensis/s. minor", "Tragopogon pratensis ssp. orientalis", "Trapeliopsis granulosa", "Trichophorum cespitosum s. germanicum", "Trichostomum crispulum", "Trifolium arvense", "Trifolium campestre", "Trifolium dubium", "Trifolium medium", "Trifolium pratense", "Trifolium scabrum", "Trifolium striatum", "Trifolium subterraneum", "Triglochin maritima", "Tripleurospermum maritimum", "Tripolium pannonicum", "Trisetum flavescens", "Typha latifolia", "Urtica dioica", "Utricularia australis", "Utricularia intermedia", "Utricularia minor", "Utricularia vulgaris", "Vaccinium myrtillus", "Vaccinium oxycoccos", "Vaccinium uliginosum", "Vaccinium vitis-idaea", "Valeriana dioica", "Valeriana officinalis", "Verbascum nigrum", "Veronica anagallis-aquatica", "Veronica austriaca s. teucrium", "Veronica catenata", "Veronica chamaedrys", "Veronica hederifolia", "Veronica hederifolia s. lucorum", "Veronica montana", "Veronica officinalis", "Veronica prostrata", "Veronica scutellata", "Veronica serpyllifolia", "Veronica verna", "Viburnum opulus", "Vicia cracca", "Vicia lathyroides", "Vicia sepium", "Vincetoxicum hirundinaria", "Viola canina", "Viola hirta", "Viola lutea s. calaminaria", "Viola odorata", "Viola palustris", "Viola reichenbachiana", "Viola reichenbachiana/riviniana", "Viola riviniana", "Viola rupestris", "Viola stagnina", "Viola tricolor subsp. curtisii", "Warnstorfia fluitans", "Xanthium orientale", "Zannichellia palustris s. palustris", "Zannichellia palustris s. pedicellata", "Zannichellia palustris s.l.", "Zygogonium ericetorum"]
# lst2 = set([simplify_species(s) for s in lst])
# pd.Series(list(lst2)).sort_values().to_clipboard(index=False)
#
# pivot = pd.read_excel(r'c:\Users\roelo008\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP06_soorenselectie_habitat\referentie_doren1\relaties_tussen_de_hoeveelheid_stikstofdepositie_-hydrotheek_(stowa)_547784.xlsx',
#                       sheet_name='Sheet1_copy')
# pivot2 = pivot.drop(columns=['soortnr', 'Nederlandse naam', 'EU-naam', 'count'])
# flat = pd.melt(pivot2.drop_duplicates(subset=['wetenschappelijke naam']), id_vars='wetenschappelijke naam').dropna(subset='value')
# flat['Wat'] = flat.value.map({1: 'kwalificerend', 2: 'kwalificerend en verdringend', 3: 'verdringend'})
# flat['Habitattype'] = 'H' + flat.variable.str.replace('-', '_').fillna(flat.variable.astype(str))
# flat.to_clipboard(index=False, sep=',')
