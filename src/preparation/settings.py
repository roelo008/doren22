import os

SOURCE_DATA = {
    "doren_master_excel": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
        os.environ.get("username")
    ),
    "eva_headers": r"w:\PROJECTS\Doren19\a_brondata\EVA\delivery_20201118\EVA_Doren_header.csv",
    "eva_species": r"w:\PROJECTS\Doren19\a_brondata\EVA\delivery_201909\EVA_Doren_species.csv",
    "aoi": r"W:\PROJECTS\Doren19\a_brondata\AOI\ne_50m_cntrs_AOI_diss_fin.shp",
    "elevation": r"W:\PROJECTS\Doren19\a_brondata\covariables\DEM\DTM_3035.tif",
    "soil_map": r"w:\PROJECTS\Doren19\a_brondata\covariables\soil\b_processed\WRBLEV1_laea.tif",
    "simplified_soil_types": {
        "src": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
            os.environ.get("username")
        ),
        "sheet": "Bodemtypes_versimpeld",
        "key": "Specifieke bodemtypes",
        "value": "Versimpelde bodemtypes",
    },
    "country_map": r"w:\PROJECTS\Doren19\a_brondata\covariables\countries\ne_50m_cntrs_sel_buff_diss_2_3035.shp",
    "country_background_map": r"w:\PROJECTS\DOREN22\a_sourcedata\eu_countries\background_countries\ne_110m_admin0_doren_countries_v2.shp",
    "temperature_directory": r"w:\PROJECTS\Doren19\a_brondata\covariables\EObs\2_compiled\epsg3035",
    "temperature_basename": r"EObs_v200e_tg_5yrmean",
    "precipitation_directory": r"w:\PROJECTS\Doren19\a_brondata\covariables\EObs\2_compiled\epsg3035",
    "precipitation_basename": r"EObs_v200e_rr_5yrmean",
    "nh3_averaged": {
        "30_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NH3_a_30-20221230.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NH3_f_30-20221230.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NH3_v_30-20221230.csv",
        },
        "20_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NH3_a_20-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NH3_f_20-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NH3_v_20-20230101.csv",
        },
        "10_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NH3_a_10-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NH3_f_10-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NH3_v_10-20230101.csv",
        },
        "5_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NH3_a_05-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NH3_f_05-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NH3_v_05-20230101.csv",
        },
        "2019_yr": {
            "f": r"w:\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery\NH3_f-20201012.csv",
            "v": r"w:\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery\NH3_v-20201012.csv",
        },
        "yearly": {
            "f": r"w:\projECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230515\NH3_f-20230515.csv",
            "v": r"w:\projECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230515\NH3_v-20230515.csv"  # TODO
        },
    },
    "nox_averaged": {
        "30_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NOx_a_30-20221230.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NOx_f_30-20221230.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20221230\NOx_v_30-20221230.csv",
        },
        "20_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NOx_a_20-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NOx_f_20-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230108\NOx_v_20-20230101.csv",
        },
        "10_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NOx_a_10-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NOx_f_10-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230103\NOx_v_10-20230101.csv",
        },
        "5_yr": {
            "a": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NOx_a_05-20230101.csv",
            "f": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NOx_f_05-20230101.csv",
            "v": r"w:\PROJECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230102\NOx_v_05-20230101.csv",
        },
        "2019_yr": {
            "f": r"w:\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery\NOx_f-20201012.csv",
            "v": r"w:\PROJECTS\Doren19\a_brondata\POSCH_dep\20201012delivery\NOx_v-20201012.csv",
        },
        "yearly": {
            "f": r"w:\projECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230515\NOx_f-20230515.csv",  # TODO
            "v": r"w:\projECTS\DOREN22\b_prepareddata\EMEP_averaged\max_20230515\NOx_v-20230515.csv",  # TODO
        },
    },
    "structuurtypen_eunis_nieuw": {
        "src": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
            os.environ.get("username")
        ),
        "sheet": "EUNIS_structuur",
        "key": "eunis",
        "value": "type",
    },
    "ruwheid_eunis_nieuw": {
        "src": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
            os.environ.get("username")
        ),
        "sheet": "EUNIS_structuur",
        "key": "eunis",
        "value": "hoog_laag",
    },
    "structuurtypen_eunis_oud": {
        "src": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
            os.environ.get("username")
        ),
        "sheet": "oude_EUNIS_structuur",
        "key": "EUNIS_OLD",
        "value": "type",
    },
    "ruwheid_eunis_oud": {
        "src": r"c:\Users\{0}\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP01_inzicht\DOREN_2022_masterfile.xlsx".format(
            os.environ.get("username")
        ),
        "sheet": "oude_EUNIS_structuur",
        "key": "EUNIS_OLD",
        "value": "hoog_laag",
    },
}

EUNIS_OVERWRITE = {
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Calluna_Avenula.csv": "S42",
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Calluna_Molinea.csv": "S42",
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Empetrum_Avenula.csv": "S42",
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Empetrum_Molinea.csv": "S42",
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Erica_Avenula.csv": "S41",
    r"W:\PROJECTS\Doren19\a_brondata\EVA\delivery_20210112\Erica_Molinea.csv": "S41",
}

FILTERS = {
    "earliest_year": 1950,
    "latest_year": 2020,
    "maximum_elevation": 500,
}

PREPARED_DATA = {
    "prepared_headers": r"w:\projects\DOREN22\b_prepareddata\eva_headers\20230222-1254_eva_headers.csv",
    "eva_species": r'W:\PROJECTS\DOREN22\b_prepareddata\eva_headers\20230222-1254_eva_species.csv'
}

BASE_OUT_DIRECTORY = r"w:\PROJECTS\DOREN22\b_prepareddata\eva_headers"

DOREN1_REFERENCE_DATA = f"c:\\Users\\{os.environ.get('username')}\\Wageningen University & Research\\DOREN 2022 - fase01 - verkenning\\WP06_soorenselectie_habitat\\referentie_doren1\\relaties_tussen_de_hoeveelheid_stikstofdepositie_-hydrotheek_(stowa)_547784.xlsx"

EVA_HEADERS_SPECIES_IDENTIFIER_COLUMN = 'matched_concept'