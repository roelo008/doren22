import os
import sys
import pandas as pd
import numpy as np
import pathlib
import datetime

sys.path.append(str(pathlib.Path(__file__).parents[1]))
try:
    from interactive_analysis_species.header_queries import DorenPlots, ckdnearest
    from preparation.settings import SOURCE_DATA, PREPARED_DATA
    from preparation.header_management_functions import (
        slugify,
        species_name_translator,
        dict_from_file,
    )
except ModuleNotFoundError:
    from src.interactive_analysis_species.header_queries import DorenPlots, ckdnearest
    from src.preparation.settings import SOURCE_DATA, PREPARED_DATA
    from src.preparation.header_management_functions import (
        slugify,
        species_name_translator,
        dict_from_file,
    )


def get_all_structuurtypen():
    mapper_part1 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_nieuw"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_nieuw"]["key"],
        value_column=SOURCE_DATA["structuurtypen_eunis_nieuw"]["value"],
        sheet=SOURCE_DATA["structuurtypen_eunis_nieuw"]["sheet"],
    )

    mapper_part2 = dict_from_file(
        src=SOURCE_DATA["structuurtypen_eunis_oud"]["src"],
        key_column=SOURCE_DATA["structuurtypen_eunis_oud"]["key"],
        value_column=SOURCE_DATA["structuurtypen_eunis_oud"]["value"],
        sheet=SOURCE_DATA["structuurtypen_eunis_oud"]["sheet"],
    )

    return list(set(list(mapper_part1.values()) + list(mapper_part2.values())))


def species_data_file_testing(plots: DorenPlots,
                              identifier: str) -> (str, pd.DataFrame):
    """
    As species_data_file, but just report on number of matched plots and return df with plot IDs
    for testing purposes!

    Parameters
    ----------
    plots
    identifier
    Returns
    -------

    """

    # Identify plots with the species
    df = plots.get_plots_inventory_for_species(species_identifier=identifier)
    s = f'{identifier} - {df.shape[0]} based on {plots.preparation_menu["prepared_headers"]}\n'

    return s, df


def species_data_file(
    plots: DorenPlots,
    identifier: str,
) -> pd.DataFrame:
    """
    Build a dataframe for a species containing all headers in the selected headers dataframe
    Columns are:
      plot_obs_id: identifier
      respons: 1/0 plot contains species or not
      coverage: bedekking van de soort in het plot in %
      dist2nearest_+_<structuurtype>: distance to nearst POSITIVE plot of <structuurtype>

    Parameters
    ----------
    plots: DorenPlots object carrying all selected EVA plots
    identifier: species name

    Returns
    -------
    pd.DataFrame
    """

    # identifier = r'Allium oleraceum'
    # structuurtype = 'nat loofbos'

    print(f"building species response file for {identifier}.")

    # Identify plots with the species
    plots.identify_headers_with_species(species_identifier=identifier,
                                        shout=True)

    # Get bedekking as series. Query the Species inventory dataframe for:
    #   First the selected plot-observation IDs
    #   Secondly the requested species within these plots
    # Possibly, a species is listed > once in a plot. To account for this, use a group-by summary.
    bedekking = (
        plots.species_df.loc[
            (
                plots.species_df.plot_obs_id.isin(
                    plots.header_gdf.loc[plots.header_gdf.has_species].index
                )
            )
            & ((plots.species_df.matched_concept == identifier) |
               (plots.species_df.matched_concept_simplified == identifier))
        ]
        .groupby("plot_obs_id").cover_percentage.mean().rename("coverage")
    )

    # Voor elk structuurtype, bepaal de afstand van elk Negatief plot, naar dichstbijzijnst Positieve plot van
    # dat structuurtype
    distance_lst = []
    origin_points = plots.header_gdf.loc[~plots.header_gdf.has_species]
    for structuurtype in get_all_structuurtypen():
        destination_points = plots.header_gdf.loc[
            (plots.header_gdf.structuur == structuurtype)
            & (plots.header_gdf.has_species)
        ]
        if destination_points.empty:
            dist_2_nearest = pd.Series(
                [np.nan] * origin_points.shape[0], index=origin_points.index
            )
        else:
            nearest_ids, dist_2_nearest = ckdnearest(
                gdA=origin_points, gdB=destination_points
            )

        distance_lst.append(
            dist_2_nearest.round(2).rename(f"dist2nearest_+_{structuurtype}")
        )

    # Compile as single dataframe
    return (
        pd.concat(
            [plots.header_gdf.loc[:, "has_species"].map({True: 1, False: 0}), bedekking]
            + distance_lst,
            axis=1,
        )
        .rename(columns={"has_species": "response"})
        .fillna({"bedekking": 0})
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("out_dir", help="output_directory", type=str)
    parser.add_argument("--ids", help="species identifier in Latin", type=str, nargs="+")
    parser.add_argument("--id_src", help="csv file with species identifier list", type=str)
    parser.add_argument('--test', help='create testing files only', action='store_true')
    args = parser.parse_args()

    if args.id_src:
        ids = pd.read_csv(args.id_src, encoding='cp1252')
        ids = ids.iloc[:, 0].to_list()
    else:
        ids = args.ids

    plots = DorenPlots(source_data=SOURCE_DATA, prepared_data=PREPARED_DATA)

    for species in ids:
        out_file_name = f'{slugify(species)}_plot_response_file.csv'
        with open(os.path.join(args.out_dir, out_file_name), "w") as f:
            if args.test:
                msg, df = species_data_file_testing(plots=plots, identifier=species)
                f.write(msg)
                f.write(df.to_csv(sep=',', lineterminator='\n', index=False))
            else:
                df = species_data_file(plots=plots, identifier=species)
                msg = f'# {species} response file based on: {plots.preparation_menu["prepared_headers"]}, created {datetime.datetime.now().strftime("%Y-%m-%d %H:%M")}\n'
                f.write(
                    msg
                )
                f.write(df.to_csv(index_label="plot_obs_id", sep=",", lineterminator="\n"))
