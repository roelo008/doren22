import pandas as pd

df = pd.read_excel(r'c:\Users\roelo008\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP06_soorenselectie_habitat\soortenlijst_varianten_per_habtype_20231031-1401.xlsx',
                   sheet_name='groslijst')
df['soortnaam'] = df.synoniem.fillna(df.wetenschappelijke_naam)

habtypen = list(set(df.Habitattype))
master = 'variant01'
sub = 'variant03'


for habtype in habtypen:

    master_set = set(df.loc[(df[master]) & (df.Habitattype == habtype), 'soortnaam'])
    test_set = set(df.loc[(df[sub]) & (df.Habitattype == habtype), 'soortnaam'])
    issubset = test_set.issubset(master_set)
    print(f'{habtype}: {sub} is subset of {master}: {issubset}')