import re
import os
import datetime

import numpy as np
import pandas as pd

from src.preparation.settings import DOREN1_REFERENCE_DATA, PREPARED_DATA

"""
Dit script bouwt een Excel file waarin de GEVRAAGDE soorten worden vergeleken met de soorten uit EVA. 

"""


OUTPUT_DIR = r"c:\Users\roelo008\Wageningen University & Research\DOREN 2022 - fase01 - verkenning\WP06_soorenselectie_habitat"
HEADER_COUNT_THRESHOLD = (
    100  # minimum nr of headers in <timestamp>_eva_headers.csv containing the species
)

# Read brondata
doren1_kwalificerend = pd.read_excel(DOREN1_REFERENCE_DATA, sheet_name="Sheet1_flat")

habitattype_typisch = pd.read_excel(
    DOREN1_REFERENCE_DATA, sheet_name="habitattype_typische_soorten"
).assign(Wat="typisch")

synoniemen = (
    pd.read_excel(DOREN1_REFERENCE_DATA, sheet_name="soortnaam_vertalingen")
    .dropna(subset=["soortnaam_in_EVA"])
    .set_index("soortnaam_gevraagd")
    .soortnaam_in_EVA.to_dict()
)

rode_lijst = pd.read_excel(DOREN1_REFERENCE_DATA, sheet_name="rode_lijst_flora")

counts_file = os.path.join(
    os.path.dirname(PREPARED_DATA["prepared_headers"]),
    f"{os.path.basename(PREPARED_DATA['prepared_headers'])[:18]}species_lists.csv",
)
counts_w_subspecies = pd.read_csv(
    counts_file,
    usecols=["matched_concept", "pre_filter", "post_filter"],
)
counts_w_subspecies.set_index("matched_concept", drop=True, inplace=True)

counts_wo_subspecies = pd.read_csv(
    counts_file,
    usecols=["simplified", "pre_filter_simplified", "post_filter_simplified"],
    index_col="simplified",
).dropna(subset=["post_filter_simplified"], how="all")

# Concatenate kwalifcierende en typische informatie
dat = pd.concat(
    [
        doren1_kwalificerend.loc[:, ["wetenschappelijke_naam", "Habitattype", "Wat"]],
        habitattype_typisch.loc[:, ["wetenschappelijke_naam", "Habitattype", "Wat"]],
    ],
    ignore_index=True,
)

# Add alternatieve naam. Alternatieve namen zijn ALLEEN bepaald voor gevraagde soorten die 0x voorkomen in de EVA database.
dat["wetenschappelijke_naam_synoniem"] = dat.wetenschappelijke_naam.map(
    synoniemen
).fillna(dat.wetenschappelijke_naam)

# s.l. sensu-lato in brede zin. Verwijder deze aanduiding, zodat de originele soortnaam wordt gebruikt
dat.wetenschappelijke_naam_synoniem = dat.wetenschappelijke_naam_synoniem.str.replace(" s.l.", "")

# Harmoniseer diverse subsoort-aanduidingen naar "subsp.". Doe dit in de synoniem kolom, zodat originele naam zichtbaar blijft
dat["wetenschappelijke_naam_synoniem"] = dat.wetenschappelijke_naam_synoniem.str.replace(
    "( ssp\. | subsp\. | var. | s\. )", " subsp. ", regex=True
)

# Add counts of each species in the EVA database. Distinguish count pre- and post filtering of EVA database
# Subspecies are searched for in EVA database as is. Example:
#     Gevraagd: Festuca ovina ssp. hirtula -->
#     In EVA:   Festuca ovina subsp. hirtula pre_filter: 236 post_filter: 112.0
#
# Species are searched for in EVA database INCLUDING subspecies. Example:
#     Gevraagd: Galium pumilum
#     In EVA:
#               matched_concept                     pre_filter  post_filter
#               Galium pumilum                      16718       2114
#               Galium pumilum subsp. marchandii    152         0
#               Galium pumilum subsp. papillosum    84          2
#               Galium pumilum subsp. pinetorum     691         34
#               Galium pumilum subsp. pumilum       11          0
#               Galium pumilum subsp. rivulare      116         0
#    eva_count_pre_filter = 16718+152+84+691+11+116 = 17.772
#    eva_count_post_filter = 2114+0+2+34+0+1 = 2.150

counts = pd.merge(
    dat,
    counts_w_subspecies,
    left_on="wetenschappelijke_naam_synoniem",
    right_index=True,
    how="left",
).fillna(dict(zip(list(counts_w_subspecies), [0] * counts_w_subspecies.shape[1])))

counts = pd.merge(
    counts,
    counts_wo_subspecies,
    left_on="wetenschappelijke_naam_synoniem",
    right_index=True,
    how="left",
).fillna(dict(zip(list(counts_wo_subspecies), [0] * counts_wo_subspecies.shape[1])))

# Add highest EVA  counts.
count_pre_filter_column = "eva_count_pre_filter"
count_post_filter_column = "eva_count_post_filter"
dat[count_pre_filter_column] = (
    counts.loc[:, [c for c in list(counts) if "pre" in c]].max(axis=1).fillna(0)
)
dat[count_post_filter_column] = (
    counts.loc[:, [c for c in list(counts) if "post" in c]].max(axis=1).fillna(0)
)

# Add redlist indicator
rode_lijst_column = "rode_lijst"
dat[rode_lijst_column] = dat.wetenschappelijke_naam.isin(
    rode_lijst.set_index("WETENSCHAPPELIJKE NAAM").index
) | dat.wetenschappelijke_naam_synoniem.isin(
    rode_lijst.set_index("WETENSCHAPPELIJKE NAAM").index
)

# Define queries for each variant
queries = {
    "variant01": f"Wat == 'kwalificerend' & {count_post_filter_column} >= {HEADER_COUNT_THRESHOLD}",
    "variant02": f"Wat == 'kwalificerend' & {count_post_filter_column} >= {HEADER_COUNT_THRESHOLD} & {rode_lijst_column} == True",
    "variant03": f"Wat == 'typisch' & {count_post_filter_column} >= {HEADER_COUNT_THRESHOLD}",
    "variant04": f"Wat == 'typisch' & {count_post_filter_column} >= {HEADER_COUNT_THRESHOLD} & {rode_lijst_column} == True",
}

#  Identify rows that comply to each query
for k, v in queries.items():
    dat[k] = dat.index.isin(dat.query(v).index)

# Write contents to Excel file
with pd.ExcelWriter(
    os.path.join(
        OUTPUT_DIR,
        f'soortenlijst_varianten_per_habtype_{datetime.datetime.now().strftime("%Y%m%d-%H%M")}.xlsx',
    )
) as dest:
    # Metadata sheet
    pd.Series(
        {
            "created": datetime.datetime.now().strftime("%Y%m%d-%H%M"),
            "created_by": os.environ.get("username"),
            "project": "DOREN-2",
        }
    ).to_excel(dest, sheet_name="metadata")

    # Uitleg groslijst
    pd.Series(
        {
            "wetenschappelijke naam": 'wetenschappelijke naam, inclusief subspecies. Subspecies aanduiding geharmonineerd naar "ssp."',
            "synoniem": f'alternatieve naam voorgedragen door WW en FvdZ, zie {DOREN1_REFERENCE_DATA} - sheetname "soortnaam vertalingen".',
            "Habitattype": "habitattype code",
            "Wat": f"Kwalificatie van de soort in habitattype: Doren1 Kwalificerend, Doren1 Verdringend, Doren1VerdringenEnKwalificerend, Habitattype-typisch. Gebaseerd op {DOREN1_REFERENCE_DATA}.",
            "eva_count_pre_filter": f"aantal plots in EVA database met deze soort, pre filtering, gebaseerd op {counts_file}.",
            "eva_count_post_filter": f"aantal plots in EVA database met deze soort, post filtering, gebaseerd op {counts_file}",
            "rode_lijst": f'Rode lijst True/False indicator, gebaseerd op {DOREN1_REFERENCE_DATA}-sheet "rode_lijst_flora"',
            "VARIANTXX": "TRUE/FALSE voor deze variant",
            "VARIANT01": f"Alle Doren1 kwalificerende soorten (excl kwalificerend EN verdringend) en eva_count_post_filter >= {HEADER_COUNT_THRESHOLD}.",
            "VARIANT02": "Zoals VARIANT01 en moet rode-lijst zijn.",
            "VARIANT03": f"Alle Habitattype-typische soorten en eva_count_post_filter >= {HEADER_COUNT_THRESHOLD}.",
            "VARIANT04": "Zoals VARIANT03 en moet rode-lijst zijn.",
        }
    ).to_excel(dest, sheet_name="groslijst_uitleg")

    # Groslijst. Create new column with synoniemen only when different from original
    dat['synoniem'] = np.where(dat.wetenschappelijke_naam == dat.wetenschappelijke_naam_synoniem,
                               np.nan, dat.wetenschappelijke_naam_synoniem)

    dat.sort_values(by=["wetenschappelijke_naam", "Habitattype", "Wat"]).loc[
        :,
        [
            "wetenschappelijke_naam",
            "synoniem",
            "Habitattype",
            "rode_lijst",
            "Wat",
            "eva_count_pre_filter",
            "eva_count_post_filter",
        ]
        + list(queries.keys()),
    ].to_excel(
        dest, sheet_name="groslijst", index=False, freeze_panes=(1, 0)
    )

    # Pivot table for elke variant
    for v, q in queries.items():
        piv = pd.pivot_table(
            dat.query(q),
            index="wetenschappelijke_naam_synoniem",
            columns="Habitattype",
            values="Wat",
            aggfunc="count",
        )
        piv.T.to_excel(dest, sheet_name=f"pivot_{v}")

    # Habitattype-Variant counts
    dat.loc[
        :, ["Habitattype", "variant01", "variant02", "variant03", "variant04"]
    ].groupby("Habitattype").sum().sort_index().to_excel(
        dest, sheet_name="summary_per_habitattype"
    )

    # Alle unieke soortnamen die in tenminste een variant voorkomen. Gebruik synoniemen, want die zitten altijd in EVA.
    # Dataframe with unique species that comply to >=1 variant(en)
    uniek_gevraagd = dat.loc[
        (dat.variant01) | (dat.variant02) | (dat.variant03) | (dat.variant04),
        "wetenschappelijke_naam",
    ].drop_duplicates(ignore_index=True)
    uniek_matched = uniek_gevraagd.map(
        dat.set_index(
            "wetenschappelijke_naam"
        ).wetenschappelijke_naam_synoniem.to_dict()
    )
    # uniek_as_in_eva = uniek_matched.map(counts_w_subspecies.matched_concept.to_dict())
    pd.DataFrame(
        {
            "gevraagd": uniek_gevraagd,
            "matchend": uniek_matched,
        }
    ).sort_values(by="gevraagd").to_excel(dest, index=False, sheet_name="soortenlijst_voor_EVA_queries")
